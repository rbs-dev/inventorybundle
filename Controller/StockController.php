<?php

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Service\FormValidationManager;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Reader\Html;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Entity\ItemUnit;
use Terminalbd\GenericBundle\Form\ItemFormType;
use Terminalbd\GenericBundle\Repository\CategoryRepository;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\CmpPrice;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\InventoryBundle\Form\InventoryFilterFormType;
use Terminalbd\InventoryBundle\Form\PurchaseFilterFormType;
use Terminalbd\InventoryBundle\Form\StockFormType;
use Terminalbd\InventoryBundle\Repository\StockBookRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;
use Terminalbd\InventoryBundle\Repository\StockWearhouseRepository;


/**
 * @Route("/inv/stock")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class StockController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            100  /*limit per page*/
        );
        return $pagination;
    }


    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/", methods={"GET", "POST"}, name="inv_stock")
     */
    public function index(Request $request , InventoryRepository $inventoryRepository, GenericMasterRepository $masterRepository, StockRepository $repository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $genericConfig = $masterRepository->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(InventoryFilterFormType::class , null,array('config'=>$genericConfig,'categoryRepo'=>$categoryRepo))->add('Excel', SubmitType::class);
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$data);
        } else {
            $search = $repository->findBySearchQuery($config,$data);
        }
        unset($_SESSION['previousPage']);
        $uri = $request->getUri();
        $this->get('session')->set('previousPage', $uri);
        $previousPageUrl = $this->get('session')->get('previousPage');
        $pagination = $this->paginate($request,$search);
        if($searchForm->get('Excel')->isClicked()){
            $search = $repository->findBySearchQuery($config,$data);
           // dd($search->getArrayResult());
            $html = $this->renderView('@TerminalbdInventory/stock/index-excel.html.twig',[
                'pagination' => $search->getArrayResult()
            ]);
        //    dd($html);
            $spreadsheet = new Spreadsheet();
            $reader = new Html();
            $spreadsheet = $reader->loadFromString($html);
            ob_end_clean();
            $fileName = $request->get('_route') . '_' . time() . '.xlsx';
            header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
            header("Content-Disposition: attachment;filename=$fileName");
            header('Cache-Control: max-age=0');
            $writer = IOFactory::createWriter($spreadsheet,'Xlsx');
            $writer = new Xlsx($spreadsheet);
            exit($writer->save('php://output'));
        }
        return $this->render('@TerminalbdInventory/stock/index.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }


     /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/book", methods={"GET", "POST"}, name="inv_stock_book")
     */
    public function stockBook(Request $request,StockBookRepository $repository, InventoryRepository $inventoryRepository, GenericMasterRepository $masterRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $genericConfig = $masterRepository->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(InventoryFilterFormType::class , null,array('config'=>$genericConfig,'categoryRepo'=>$categoryRepo));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$data);
        } else {
            $search = $repository->findBySearchQuery($config,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdInventory/stock/stock-book.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_PROCUREMENT_STORE')")
     * @Route("/wearhouse", methods={"GET", "POST"}, name="inv_stock_wearhouse")
     */
    public function stockWearhouse(Request $request,StockWearhouseRepository $repository, InventoryRepository $inventoryRepository, GenericMasterRepository $masterRepository): Response
    {

        $terminal = $this->getUser()->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $genericConfig = $masterRepository->config($terminal->getId());
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $searchForm = $this->createForm(InventoryFilterFormType::class , null,array('config'=>$genericConfig,'categoryRepo'=>$categoryRepo));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$data);
        } else {
            $search = $repository->findBySearchQuery($config,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdInventory/stock/stock-wearhouse.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/xx", methods={"GET", "POST"}, name="inv_stock_history_book")
     */
    public function stockBookHistory(Request $request): Response
    {
        return $this->render('@TerminalbdInventory/stock/stock-history.html.twig');
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/new", methods={"GET", "POST"}, name="inv_stock_new")
     */
    public function new(Request $request, TranslatorInterface $translator, GenericMasterRepository $masterRepository, StockRepository $repository, InventoryRepository $inventoryRepository,StockBookRepository $stockBookRepository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $invConfig = $inventoryRepository->config($terminal);
        $entity = new Item();
        $data = $request->request->all();
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(ItemFormType::class, $entity,array('config'=>$config,'categoryRepo'=>$categoryRepo));
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $count = $this->getDoctrine()->getRepository(Item::class)->count(['config'=>$config,'name' => $entity->getName()]);
            if($count == 0){
                $em = $this->getDoctrine()->getManager();
                if(empty($entity->getUnit())){
                    $pcs = $this->getDoctrine()->getRepository(ItemUnit::class)->findOneBy(array('slug'=>"pcs"));
                    $entity->setUnit($pcs);
                }
                $entity->setConfig($config);
                $em->persist($entity);
                $em->flush();
                $repository->insertStockItem($invConfig,$entity);
                $stockBookRepository->insertMasterStockItem($entity,$data);
                $this->addFlash('success',$translator->trans('data.created_successfully'));
                return $this->redirectToRoute('inv_stock_edit',array('id' => $entity->getId()));
            }else{
                $this->addFlash('notice',"This product already created, Please try another item");
            }

        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->render('@TerminalbdGeneric/item/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/build", methods={"GET", "POST"}, name="inv_stock_build")
     */
    public function build(Request $request, TranslatorInterface $translator, InventoryRepository $inventoryRepository, GenericMasterRepository $masterRepository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $inventoryRepository->config($terminal);
        $genricConfig = $masterRepository->config($terminal);
        $entity = new Stock();
        $data = $request->request->all();
        $itemRepo = $this->getDoctrine()->getRepository(Item::class);
        $form = $this->createForm(StockFormType::class, $entity,array('config'=>$genricConfig,'itemRepo'=>$itemRepo))
            ->add('SaveAndCreate', SubmitType::class);
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setConfig($config);
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('data.created_successfully'));
            return $this->redirectToRoute('inv_stock');

        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        return $this->render('@TerminalbdInventory/stock/new.html.twig', [
            'form' => $form->createView(),
        ]);
    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inv_stock_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     */
    public function edit(Request $request,$id, TranslatorInterface $translator, GenericMasterRepository $masterRepository, ItemRepository $itemRepository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $entity = $itemRepository->findOneBy(array('config' => $config,'id' => $id));
        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(ItemFormType::class, $entity,array('config'=>$config,'categoryRepo'=>$categoryRepo));
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $count = $this->getDoctrine()->getRepository(Item::class)->count(['config'=>$config,'name' => $entity->getName()]);
            if($count == 1 or $count == 0) {
                $em = $this->getDoctrine()->getManager();
                $em->persist($entity);
                $em->flush();
                $this->addFlash('success', $translator->trans('data.created_successfully'));
                $previousPageUrl = $this->get('session')->get('previousPage');
                if (!$previousPageUrl) {
                    return $this->redirectToRoute('inv_stock');
                }
                unset($_SESSION['previousPage']);
                return $this->redirect($previousPageUrl);
            }else{
                $this->addFlash('notice',"This product already created, Please try another item");
            }
        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        $barnds = $this->getDoctrine()->getRepository(ItemBrand::class)->findBy(['config' => $config,'status' => 1,'isDelete'=>0],['name'=>'ASC']);
        $sizes = $this->getDoctrine()->getRepository(ItemSize::class)->findBy(['config' => $config,'status' => 1,'isDelete'=>0],['name'=>'ASC']);
        $colors = $this->getDoctrine()->getRepository(ItemColor::class)->findBy(['config' => $config,'status' => 1,'isDelete'=>0],['name'=>'ASC']);
        return $this->render('@TerminalbdGeneric/item/new.html.twig', [
            'entity' => $entity,
            'brands' => $barnds,
            'sizes' => $sizes,
            'colors' => $colors,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET", "POST"}, name="inv_stock_reset")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function reset(GenericMasterRepository $masterRepository , InventoryRepository $inventoryRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $config = $masterRepository->config($terminal);
        $inventory = $inventoryRepository->config($terminal);
        $post = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET", "POST"}, name="inv_stock_show")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id): Response
    {
        $post = $this->getDoctrine()->getRepository(Item::class)->find($id);
        $em = $this->getDoctrine()->getManager();
        $em->remove($post);
        $em->flush();
        $this->addFlash('success', 'post.deleted_successfully');
        return new Response('Success');
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inv_stock_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     */
    public function delete(Stock $entity): Response
    {

        $em = $this->getDoctrine()->getManager();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        $date = date('d-m-Y i:h');
        $newName = "{$entity->getItem()->getName()}-{$date}";
        $entity->getItem()->setName($newName);
        $entity->setStatus(false);
        $entity->setName($newName);
        $entity->setIsDelete(true);
        $entity->getItem()->setIsDelete(true);
        $entity->getItem()->setStatus(false);
        /* @var $books StockBook */
        foreach ($entity->getItem()->getStockBooks() as $books){
            $books->setIsDelete(true);
            $books->setStatus(false);
        }
        $em->flush();
        $response = 'valid';
        return new Response($response);

    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/stock-book-delete", methods={"GET"}, name="inv_stock_book_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     */
    public function stockBookDelete(StockBook $entity): Response
    {

        $em = $this->getDoctrine()->getManager();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        $entity->setIsDelete(true);
        $em->flush($entity);
        $em->flush();
        $response = 'valid';
        return new Response($response);

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="inv_stock_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function status(Stock $entity): Response
    {

        $status = $entity->getItem()->getStatus();
        if($status == 1){
            $entity->setStatus(false);
            $entity->getItem()->setStatus(false);
            foreach ($entity->getItem()->getStockBooks() as $books){
                $books->setStatus(false);
            }
        }else{
            $entity->setStatus(true);
            $entity->getItem()->setStatus(true);
            foreach ($entity->getItem()->getStockBooks() as $books){
                $books->setStatus(true);
            }
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/category-attribute", methods={"GET","POST"}, name="inv_category_attribute" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY') or is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_PROCUREMENT')")
     */
    public function categoryAttribute(Request $request , StockRepository $stockRepository): Response
    {
        $id = $_REQUEST['id'];
        /* @var $entity Stock */
        $entity = $this->getDoctrine()->getRepository(Stock::class)->findOneBy(array('item'=>$id));
        $category = $entity->getItem()->getCategory();
        $brands = $category->getBrand();
        $sizes = $category->getSize();
        $colors = $category->getColor();
        $selectBrand = "";
        $selectBrand .= "<option value=''>---Select Brand---</option>";
        foreach ($brands as $brand){
            $selectBrand .= "<option value='{$brand->getId()}'>{$brand->getName()}</option>";
        }
        $selectSize = "";
        $selectSize .= "<option value=''>---Select Size---</option>";
        foreach ($sizes as $size){
            $selectSize .= "<option value='{$size->getId()}'>{$size->getName()}</option>";
        }

        $selectColor = "";
        $selectColor .= "<option value=''>---Select Color---</option>";
        foreach ($colors as $color){
            $selectColor .= "<option value='{$color->getId()}'>{$color->getName()}</option>";
        }

        $data = array('brand'=>$selectBrand,'size'=>$selectSize,'color'=>$selectColor);
        return new Response(json_encode($data));
    }



    /**
     * @Route("/data-table", methods={"GET", "POST"}, name="inv_stock_data_table", options={"expose"=true})
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     */

    public function dataTable(Request $request , InventoryRepository $inventoryRepository ,StockRepository $repository)
    {

        $query = $_REQUEST;

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $inventoryRepository->config($terminal);
        $iTotalRecords = $repository->count(array('config'=> $config));
        $iDisplayLength = intval($_REQUEST['length']);
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($_REQUEST['start']);
        $sEcho = intval($_REQUEST['draw']);
        $records = array();
        $records["data"] = array();
        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['name']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $search = $_POST['search']['value']; // asc or desc
        $parameter = array("offset" => $iDisplayStart,'limit'=> $iDisplayLength,'orderBy' => $columnName,"order" => $columnSortOrder,"search" => $search);

        $result = $repository->findWithSearch($config,$parameter,$query);

        $i = $iDisplayStart > 0  ? ($iDisplayStart+1) : 1;

        /* @var $post Item */

        foreach ($result as $post):
            $active = empty($post['status']) ? '' : "checked";
            $status ="<input   data-action='{$this->generateUrl('inv_stock_status',array('id'=>$post['id']))}' type='checkbox' {$active}>";
            $records["data"][] = array(
                $id                         = $i,
                $categoryName               = $post['categoryName'],
                $itemCode                   = $post['itemCode'],
                $name                       = $post['name'],
                $unitName                   = $post['unitName'],
                $purchasePrice              = $post['purchasePrice'],
                $salesPrice                 = $post['salesPrice'],
                $stockIn                    = $post['stockIn'],
                $stockOut                   = $post['stockOut'],
                $remainingQuantity          = $post['remainingQuantity'],
                $status                     = $status,
                $action                     ="<a class='btn btn-mini yellow-bg white-font' href='{$this->generateUrl('gmb_item_edit',array('id'=>$post['itemId']))}' id='{$post['itemId']}' ><i class='feather icon-edit'></i> Edit</a>
<a class='btn  btn-transparent btn-mini red-font remove' data-id='{$post['id']}' href='javascript:' data-action='{$this->generateUrl('gmb_item_delete',array('id'=>$post['id']))}' id='{$post['id']}' ><i class='feather icon-trash-2'></i></a>
");
            $i++;

        endforeach;

        if (isset($_REQUEST["customActionType"]) && $_REQUEST["customActionType"] == "group_action") {
            $records["customActionStatus"] = "OK"; // pass custom message(useful for getting status of group actions)
            $records["customActionMessage"] = "Group action successfully has been completed. Well done!"; // pass custom message(useful for getting status of group actions)
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;
        return new JsonResponse($records);
    }

    /**
     * @Route("/item-autocomplete", methods={"GET", "POST"}, name="inv_stock_item_autocomplete" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function itemAutocomplete(Request $request, GenericMasterRepository $masterRepository,ItemRepository $repository )
    {
        $allRequest = $request->query->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $data = $_REQUEST;
        $entities = $repository->getAutoComplteItemSearch($config->getId(), $data['q']);
        return new JsonResponse($entities);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/cmp-price-update", methods={"GET","POST"}, name="inv_stock_book_cmp" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function updateCmp(StockBook $entity): Response
    {

        $cmpdate = $_REQUEST['cmpdate'];
        $cmp = $_REQUEST['price'];

        $user = $this->getUser();
        $this->getDoctrine()->getRepository(CmpPrice::class)->insertCmpPrice($user,$entity,$cmp,$cmpdate);
        $entity->setCmp($cmp);
        $entity->setPrice($cmp);
        $date = new \DateTime();
        $entity->setUpdated($date);
        $entity->setCmpDate($date);
        $this->getDoctrine()->getManager()->flush();
        $data = $entity->getUpdated()->format('d-m-Y');
        return new Response($data);
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/stock-book-inline-update", methods={"GET","POST"}, name="inv_stock_book_inline_update" , options={"expose"=true})
     */
    public function stockBookInlineUpdate(Request $request ,StockBook $entity): Response
    {

        $data = $request->request->all();
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $quantity = $_REQUEST['value'];
        if($data['name'] == 'reorderQuantity'){
            $entity->setReorderQuantity($quantity);
        }
        if($data['name'] == 'minimumQuantity'){
            $entity->setMinimumQuantity($quantity);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }

     /**
     * Status a Setting entity.
     *
     * @Route("/{id}/cmp-price-history", methods={"GET","POST"}, name="inv_stock_book_cmp_history" , options={"expose"=true})
     */
    public function cmpHistory(StockBook $entity): Response
    {
        return $this->render('@TerminalbdInventory/stock/stock-book-cmp-history.html.twig',
            [
                'entity' => $entity
            ]
        );
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/cmp-price-history", methods={"GET","POST"}, name="inv_purchase_stock_book_history" , options={"expose"=true})
     */
    public function purchaseItemHistory(StockBook $entity): Response
    {
        return $this->render('@TerminalbdInventory/stock/stock-history.html.twig',
            [
                'entity' => $entity
            ]
        );
    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/update-wearhouse-inline", methods={"GET","POST"}, name="inv_stock_wearhouse_inline_update" , options={"expose"=true})
     */
    public function updateWearhouse(StockWearhouse $entity): Response
    {
        $data = $_REQUEST;
        $name = "set{$_REQUEST['name']}";
        $value = $_REQUEST['value'];
        $entity->$name($value);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * @Route("/deployment-inline-update", methods={"GET", "POST"}, name="inv_stock_inline_update", options={"expose"=true})
     */
    public function inlineUpdate(Request $request, StockRepository $repository)
    {

        $data = $request->request->all();
        $entity = $repository->find($data['pk']);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }
        $setName = 'set'.$data['name'];
        $setValue = $data['value'];
        $entity->$setName($setValue);
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');

    }


}
