<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Procurement;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Repository\Application\ProcurementRepository;
use ContainerBlo4KOZ\getPurchaseItemRepositoryService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Repository\PurchaseItemRepository;
use Terminalbd\InventoryBundle\Repository\PurchaseRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;



/**
 * @Route("/inv/purchase-item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseItemController extends AbstractController
{

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-item", methods={"GET","POST"}, name="inv_purchase_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addItem(Request $request, Purchase $entity, PurchaseRepository $purchaseRepository , PurchaseItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['purchase_item_form'];
        $itemRepository->insertStoreItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultData($purchaseRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Show a Setting entity.
     *
     * @Route("/{id}/add-issue-item", methods={"GET","POST"}, name="inv_purchase_issue_item_add" , options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */
    public function addIssueItem(Request $request, Purchase $entity, PurchaseRepository $purchaseRepository , PurchaseItemRepository $itemRepository): Response
    {
        $data = $request->request->all();
        $purchaseData = $data['purchase_garments_item_form'];
        $itemRepository->insertIssueItem($entity,$purchaseData);
        $itemRepository->getItemSummary($entity);
        $reponse = $this->returnResultIssueData($purchaseRepository,$entity->getId());
        return new Response(json_encode($reponse));
    }

    /**
     * Deletes a PurchaseItem entity.
     * @Route("/{entity}/{id}/delete-item", methods={"GET"}, name="inv_purchase_item_delete", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function deleteItem($entity , $id, InventoryRepository $inventoryRepository, PurchaseRepository $purchaseRepository, PurchaseItemRepository $itemRepository): Response
    {

        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $inventoryRepository->config($terminal);

        $purchase = $purchaseRepository->findOneBy(array('config' => $config,'id' => "{$entity}"));
        $item = $itemRepository->findOneBy(array('purchase' => $purchase ,'id' => "{$id}"));
        $em = $this->getDoctrine()->getManager();
        $em->remove($item);
        $em->flush();
        $itemRepository->getItemSummary($purchase);
        if($purchase->getInvoiceMode() == "garments"){
            $return = $this->returnResultIssueData($purchaseRepository,$item->getPurchase()->getId());
        }else{
            $return = $this->returnResultData($purchaseRepository,$item->getPurchase()->getId());
        }
        $return = $this->returnResultData($purchaseRepository,$purchase->getId());
        return new Response(json_encode($return));
    }

    /**
     * Update a PurchaseItem entity.
     * @Route("/update-item", methods={"GET","POST"}, name="inv_purchase_item_update", options={"expose"=true})
     * @Security("is_granted('ROLE_PROCUREMENT') or is_granted('ROLE_DOMAIN')")
     */

    public function updateItem(Request $request,PurchaseRepository $purchaseRepository,PurchaseItemRepository $itemRepository): Response
    {

        /* @var $item  PurchaseItem */
        $data = $request->request->all();
        $id = $data['item'];
        $item = $itemRepository->find($id);
        $itemRepository->updateStoreItem($item,$data['quantity']);
        $itemRepository->getItemSummary($item->getPurchase());
        if($item->getPurchase()->getInvoiceMode() == "garments"){
            $return = $this->returnResultIssueData($purchaseRepository,$item->getPurchase()->getId());
        }else{
            $return = $this->returnResultData($purchaseRepository,$item->getPurchase()->getId());
        }
        return new Response(json_encode($return));
    }
    
    public function returnResultData(PurchaseRepository $purchaseRepository ,$purchase){

        $entity = $purchaseRepository->find($purchase);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdInventory/purchase/purchaseItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }

    public function returnResultIssueData(PurchaseRepository $purchaseRepository ,$purchase){

        $entity = $purchaseRepository->find($purchase);
        $subTotal = $entity->getSubTotal() > 0 ? $entity->getSubTotal() : 0;
        $total = $entity->getTotal() > 0 ? $entity->getTotal() : 0;
        $html = $this->renderView(
            '@TerminalbdInventory/purchase-issue/purchaseItem.html.twig', array(
                'entity' => $entity,
            )
        );
        $data = array(
            'subTotal' => $subTotal,
            'total' => $total,
            'invoiceItem' => $html
        );
        return $data;

    }


}
