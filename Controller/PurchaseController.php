<?php

namespace Terminalbd\InventoryBundle\Controller;

use App\Entity\Application\GenericMaster;
use App\Repository\Application\GenericMasterRepository;
use App\Repository\Application\InventoryRepository;
use App\Service\FormValidationManager;
use App\Service\PaginatorService;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Repository\ItemRepository;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Form\PurchaseFilterFormType;
use Terminalbd\InventoryBundle\Form\PurchaseFormType;
use Terminalbd\InventoryBundle\Form\PurchaseGarmentsItemFormType;
use Terminalbd\InventoryBundle\Form\PurchaseItemFormType;
use Terminalbd\InventoryBundle\Form\StockFormType;
use Terminalbd\InventoryBundle\Repository\PurchaseRepository;
use Terminalbd\InventoryBundle\Repository\StockRepository;


/**
 * @Route("/inv/purchase")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseController extends AbstractController
{

    public function paginate(Request $request ,$entities)
    {

        $paginator  = $this->get('knp_paginator');
        $pagination = $paginator->paginate(
            $entities,
            $request->query->get('page', 1)/*page number*/,
            25  /*limit per page*/
        );
        return $pagination;
    }

    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/", methods={"GET", "POST"}, name="inv_purchase")
     */
    public function index(Request $request, PurchaseRepository $repository, InventoryRepository $inventoryRepository): Response
    {
        $terminal = $this->getUser()->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $purchase = new Purchase();
        $searchForm = $this->createForm(PurchaseFilterFormType::class , $purchase,array('terminal'=>$terminal));
        $searchForm -> handleRequest($request);
        $data = $_REQUEST;
        if ($searchForm->isSubmitted() && $searchForm->isValid()){
            $search = $repository->findBySearchQuery($config,$data);
        } else {
            $search = $repository->findBySearchQuery($config,$data);
        }
        $pagination = $this->paginate($request,$search);
        return $this->render('@TerminalbdInventory/purchase/index.html.twig',
            [
                'pagination' => $pagination,
                'searchForm' => $searchForm->createView()
            ]
        );

    }


    /**
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     * @Route("/new", methods={"GET", "POST"}, name="inv_purchase_new")
     */
    public function new(Request $request, TranslatorInterface $translator, InventoryRepository $inventoryRepository): Response
    {
        $errors = "";
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $inventoryRepository->config($terminal);
        $entity = new Purchase();
        $em = $this->getDoctrine()->getManager();
        $entity->setConfig($config);
        $entity->setInvoiceMode('purchase');
        $entity->setCreatedBy($this->getUser());
        $em->persist($entity);
        $em->flush();
        $this->addFlash('success',$translator->trans('data.created_successfully'));
        return $this->redirectToRoute('inv_purchase_edit',array('id'=>$entity->getId()));

    }


    /**
     * Displays a form to edit an existing Post entity.
     *
     * @Route("/{id}/edit",methods={"GET", "POST"}, name="inv_purchase_edit")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     */
    public function edit(Request $request,$id, TranslatorInterface $translator, GenericMasterRepository $masterRepository, InventoryRepository $inventoryRepository, PurchaseRepository $purchaseRepository, ItemRepository $repository): Response
    {

        $errors = "";
        $terminal = $this->getUser()->getTerminal();
        $config = $inventoryRepository->config($terminal->getId());
        $genricConfig = $masterRepository->config($terminal->getId());

        $entity = $purchaseRepository->findOneBy(array('config' => $config,'id' => "{$id}"));

        $categoryRepo = $this->getDoctrine()->getRepository(Category::class);
        $form = $this->createForm(PurchaseFormType::class, $entity,array('terminal'=>$terminal))
            ->add('save', SubmitType::class);
        $form->handleRequest($request);
        $errorValidation = new FormValidationManager();
        $errors = $errorValidation->getErrorsFromForm($form);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity->setProcess("Done");
            $em->persist($entity);
            $em->flush();
            $this->addFlash('success',$translator->trans('data.created_successfully'));
            return $this->redirectToRoute('inv_purchase');

        }elseif(!empty($errors)){
            $this->addFlash('error',$errors);
        }
        $purchaseItem = new PurchaseItem();
        $itemForm = $this->createForm(PurchaseItemFormType::class, $purchaseItem,array('config'=>$genricConfig));

        return $this->render('@TerminalbdInventory/purchase/edit.html.twig', [
            'entity' => $entity,
            'form' => $form->createView(),
            'itemForm' => $itemForm->createView(),
        ]);
    }

    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/show", methods={"GET", "POST"}, name="inv_purchase_show")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function show($id,PurchaseRepository $repository): Response
    {

        $entity = $repository->find($id);
        $html = $this->renderView(
            '@TerminalbdInventory/purchase/show.html.twig', array(
                'entity' => $entity,
            )
        );
        return new Response($html);
    }


     /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/print", methods={"GET", "POST"}, name="inv_purchase_print")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function print($id,PurchaseRepository $repository): Response
    {
        $entity = $repository->find($id);
        return $this->render('@TerminalbdInventory/purchase/print.html.twig', [
            'entity' => $entity
        ]);

    }


    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/process", methods={"GET", "POST"}, name="inv_purchase_process")
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function process($id,TranslatorInterface $translator , PurchaseRepository $repository, StockRepository $stockRepository): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $repository->find($id);
        if(isset($_REQUEST['process'])) {
            $process = $_REQUEST['process'];
            $entity->setProcess($process);
            $em->persist($entity);
            $em->flush();
            if($process =="Approved"){
                $stockRepository->processPurchaseUpdateQnt($entity);
            }
            $this->addFlash('success',$translator->trans('post.approve_successfully'));
            return $this->redirectToRoute('inv_purchase');
        }else{
            $html = $this->renderView(
                '@TerminalbdInventory/purchase/show.html.twig', array(
                    'entity' => $entity,
                )
            );
            return new Response($html);
        }
    }



    /**
     * Deletes a Setting entity.
     *
     * @Route("/{id}/delete", methods={"GET"}, name="inv_purchase_delete")
     * @Security("is_granted('ROLE_DOMAIN') or is_granted('ROLE_INVENTORY_MANAGER')")
     */
    public function delete($id): Response
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $this->getDoctrine()->getRepository(MasterItem::class)->find($id);
        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Product entity.');
        }
        try {
            $em->remove($entity);
            $em->flush();
            return new Response('Data has been deleted successfully');

        } catch (ForeignKeyConstraintViolationException $e) {
            return new Response('Data has been relation another Table');
        }

    }

    /**
     * Status a Setting entity.
     *
     * @Route("/{id}/status", methods={"GET"}, name="inv_purchase_status" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_DOMAIN')")
     */
    public function status($id): Response
    {
        $repository = $this->getDoctrine()->getRepository(Particular::class);
        $entity = $repository->find($id);
        $status = $_REQUEST['status'];
        if($status == "false"){
            $entity->setStatus(false);
        }else{
            $entity->setStatus(true);
        }
        $this->getDoctrine()->getManager()->flush();
        return new Response('success');
    }

    /**
     * @Route("/item-autocomplete", methods={"GET", "POST"}, name="inv_purchase_item_autocomplete" , options={"expose"=true})
     * @Security("is_granted('ROLE_INVENTORY_MANAGER') or is_granted('ROLE_USER') or is_granted('ROLE_DOAMIN')")
     */

    public function itemAutocomplete(Request $request, GenericMasterRepository $masterRepository,ItemRepository $repository )
    {
        $allRequest = $request->query->all();
        $terminal = $this->getUser()->getTerminal()->getId();
        $config = $masterRepository->config($terminal);
        $data = $_REQUEST;
        $entities = $repository->getAutoComplteItemSearch($config->getId(), $data['q']);
        return new JsonResponse($entities);
    }


}
