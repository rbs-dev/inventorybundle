<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Application\Inventory;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Setting\Bundle\ToolBundle\Entity\ProductUnit;
use Symfony\Component\Validator\Constraints\DateTime;
use Terminalbd\GenericBundle\Entity\Item;


/**
 * StockItem
 *
 * @ORM\Table("inv_stock_wearhouse")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\StockWearhouseRepository")
 */
class StockWearhouse
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Inventory
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $config;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $wearhouse;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $item;

  
     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $stock;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook", inversedBy="wearhouse")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $stockBook;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockIn = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockOut = 0;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $quantity= 0;

     /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $issueQuantity = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $receiveQuantity = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $remainingQuantity = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $damageQuantity = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $minimumQuantity = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $reorderQuantity = 0;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getWearhouse()
    {
        return $this->wearhouse;
    }

    /**
     * @param mixed $wearhouse
     */
    public function setWearhouse($wearhouse)
    {
        $this->wearhouse = $wearhouse;
    }

    /**
     * @return mixed
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param mixed $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param mixed $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return float
     */
    public function getStockIn()
    {
        return $this->stockIn;
    }

    /**
     * @param float $stockIn
     */
    public function setStockIn($stockIn)
    {
        $this->stockIn = $stockIn;
    }

    /**
     * @return float
     */
    public function getStockOut()
    {
        return $this->stockOut;
    }

    /**
     * @param float $stockOut
     */
    public function setStockOut($stockOut)
    {
        $this->stockOut = $stockOut;
    }

    /**
     * @return float
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param float $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }

    /**
     * @return float
     */
    public function getIssueQuantity()
    {
        return $this->issueQuantity;
    }

    /**
     * @param float $issueQuantity
     */
    public function setIssueQuantity($issueQuantity)
    {
        $this->issueQuantity = $issueQuantity;
    }

    /**
     * @return float
     */
    public function getReceiveQuantity()
    {
        return $this->receiveQuantity;
    }

    /**
     * @param float $receiveQuantity
     */
    public function setReceiveQuantity($receiveQuantity)
    {
        $this->receiveQuantity = $receiveQuantity;
    }

    /**
     * @return float
     */
    public function getRemainingQuantity()
    {
        return $this->remainingQuantity;
    }

    /**
     * @param float $remainingQuantity
     */
    public function setRemainingQuantity($remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }

    /**
     * @return float
     */
    public function getDamageQuantity()
    {
        return $this->damageQuantity;
    }

    /**
     * @param float $damageQuantity
     */
    public function setDamageQuantity($damageQuantity)
    {
        $this->damageQuantity = $damageQuantity;
    }

    /**
     * @return float
     */
    public function getMinimumQuantity()
    {
        return $this->minimumQuantity;
    }

    /**
     * @param float $minimumQuantity
     */
    public function setMinimumQuantity($minimumQuantity)
    {
        $this->minimumQuantity = $minimumQuantity;
    }

    /**
     * @return float
     */
    public function getReorderQuantity()
    {
        return $this->reorderQuantity;
    }

    /**
     * @param float $reorderQuantity
     */
    public function setReorderQuantity($reorderQuantity)
    {
        $this->reorderQuantity = $reorderQuantity;
    }





}

