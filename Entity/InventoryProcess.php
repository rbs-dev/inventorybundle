<?php

namespace Terminalbd\InventoryBundle\Entity;
use App\Entity\Application\Inventory;
use App\Entity\Domain\ModuleProcessItem;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\InventoryBundle\Entity\Purchase;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\InventoryProcessRepository")
 * @ORM\Table(name="inv_process")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class InventoryProcess
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var Inventory
     *
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $config;


     /**
     * @var Purchase
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Purchase", inversedBy="procurementProcess")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $purchase;

    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;

    /**
     * @var string
     *
     * @ORM\Column(type="string",nullable=true)
     */
    private $process;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean",nullable=true)
     */
    private $isRejected;



    /**
     * @var string
     *
     * @ORM\Column(type="text",nullable=true)
     */
    private $comment;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return bool
     */
    public function isStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus(bool $status)
    {
        $this->status = $status;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig(Inventory $config): void
    {
        $this->config = $config;
    }


    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return string
     */
    public function getProcess()
    {
        return $this->process;
    }

    /**
     * @param string $process
     */
    public function setProcess(string $process)
    {
        $this->process = $process;
    }

    /**
     * @return string
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param string $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return bool
     */
    public function isRejected(): bool
    {
        return $this->isRejected;
    }

    /**
     * @param bool $isRejected
     */
    public function setIsRejected(bool $isRejected)
    {
        $this->isRejected = $isRejected;
    }

    /**
     * @return ModuleProcessItem
     */
    public function getModuleProcessItem()
    {
        return $this->moduleProcessItem;
    }

    /**
     * @param ModuleProcessItem $moduleProcessItem
     */
    public function setModuleProcessItem(ModuleProcessItem $moduleProcessItem)
    {
        $this->moduleProcessItem = $moduleProcessItem;
    }

    /**
     * @return Purchase
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @param Purchase $purchase
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;
    }





}
