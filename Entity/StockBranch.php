<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Application\Inventory;
use App\Entity\Domain\Branch;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Setting\Bundle\ToolBundle\Entity\ProductUnit;
use Symfony\Component\Validator\Constraints\DateTime;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\ProductionBundle\Entity\ProductionBatch;
use Terminalbd\ProductionBundle\Entity\ProductionBatchItem;
use Terminalbd\ProductionBundle\Entity\ProductionExpense;
use Terminalbd\ProductionBundle\Entity\ProductionInventory;
use Terminalbd\ProductionBundle\Entity\ProductionReceiveBatch;
use Terminalbd\ProductionBundle\Entity\ProductionReceiveBatchItem;

/**
 * StockItem
 *
 * @ORM\Table("inv_stock_branch")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\StockBranchRepository")
 */
class StockBranch
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;


    /**
     * @var Inventory
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $stock;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $stockBook;

     /**
     * @var Branch
     * @ORM\ManyToOne(targetEntity="App\Entity\Domain\Branch")
     **/
    private  $branch;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockIn = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockOut = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $price = 0;

    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $subTotal = 0;


    /**
     * @var float
     *
     * @ORM\Column(name="total", type="float", nullable = true)
     */
    private $total = 0;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig($config)
    {
        $this->config = $config;
    }

    /**
     * @return mixed
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param mixed $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return mixed
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param mixed $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return Branch
     */
    public function getBranch(): Branch
    {
        return $this->branch;
    }

    /**
     * @param Branch $branch
     */
    public function setBranch($branch)
    {
        $this->branch = $branch;
    }

    /**
     * @return float
     */
    public function getStockIn()
    {
        return $this->stockIn;
    }

    /**
     * @param float $stockIn
     */
    public function setStockIn( $stockIn)
    {
        $this->stockIn = $stockIn;
    }

    /**
     * @return float
     */
    public function getStockOut()
    {
        return $this->stockOut;
    }

    /**
     * @param float $stockOut
     */
    public function setStockOut( $stockOut)
    {
        $this->stockOut = $stockOut;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice( $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal( $total)
    {
        $this->total = $total;
    }






}

