<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Application\Inventory;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Setting\Bundle\ToolBundle\Entity\ProductUnit;
use Symfony\Component\Validator\Constraints\DateTime;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;


/**
 * StockItem
 *
 * @ORM\Table("inv_stock_book")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\StockBookRepository")
 */
class StockBook
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @var Inventory
     * @ORM\ManyToOne(targetEntity="App\Entity\Application\Inventory")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $config;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $stock;

     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\StockWearhouse", mappedBy="stockBook")
     **/
    protected  $wearhouse;

       /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\StockHistory", mappedBy="stockBook")
     **/
    protected  $stockHistories;

     /**
     * @ORM\OneToMany(targetEntity="Terminalbd\InventoryBundle\Entity\CmpPrice", mappedBy="stockBook")
     **/
    protected  $cmpPrices;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item", inversedBy="stockBooks")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $item;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Category",inversedBy="stockBooks")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $category;


    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemSize")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $size;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemColor")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $color;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemBrand")
     * @ORM\OrderBy({"name" = "ASC"})
     **/
    private $brand;


    /**
     * @var string
     *
     * @ORM\Column(type="string",unique=true)
     */
    private $stockBookId;

     /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $reorderQuantity=0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $minimumQuantity=0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $cmp;


     /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $price = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockIn = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $stockOut = 0;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $remainingQuantity = 0;


    /**
     * @var float
     * @ORM\Column(type="float", nullable = true)
     */
    private $subTotal = 0;

    /**
     * @var boolean
     *
     * @ORM\Column(name="isDelete", type="boolean")
     */
    private $isDelete = false;

    /**
     * @var boolean
     *
     * @ORM\Column(name="status", type="boolean")
     */
    private $status = true;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $total = 0;

    /**
     * @var \DateTime
     * @ORM\Column(name="cmpDate", type="datetime", nullable=true)
     */
    private $cmpDate;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return Inventory
     */
    public function getConfig()
    {
        return $this->config;
    }

    /**
     * @param Inventory $config
     */
    public function setConfig(Inventory $config)
    {
        $this->config = $config;
    }

    /**
     * @return ItemSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param ItemSize $size
     */
    public function setSize($size)
    {
        $this->size = $size;
    }

    /**
     * @return ItemColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param ItemColor $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return ItemBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param ItemBrand $brand
     */
    public function setBrand($brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return Category
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * @param Category $category
     */
    public function setCategory($category)
    {
        $this->category = $category;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice( $price)
    {
        $this->price = $price;
    }

    /**
     * @return float
     */
    public function getStockIn()
    {
        return $this->stockIn;
    }

    /**
     * @param float $stockIn
     */
    public function setStockIn( $stockIn)
    {
        $this->stockIn = $stockIn;
    }

    /**
     * @return float
     */
    public function getStockOut()
    {
        return $this->stockOut;
    }

    /**
     * @param float $stockOut
     */
    public function setStockOut( $stockOut)
    {
        $this->stockOut = $stockOut;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return float
     */
    public function getReorderQuantity()
    {
        return $this->reorderQuantity;
    }

    /**
     * @param float $reorderQuantity
     */
    public function setReorderQuantity( $reorderQuantity)
    {
        $this->reorderQuantity = $reorderQuantity;
    }

    /**
     * @return float
     */
    public function getMinimumQuantity()
    {
        return $this->minimumQuantity;
    }

    /**
     * @param float $minimumQuantity
     */
    public function setMinimumQuantity( $minimumQuantity)
    {
        $this->minimumQuantity = $minimumQuantity;
    }

    /**
     * @return string
     */
    public function getStockBookId()
    {
        return $this->stockBookId;
    }

    /**
     * @param string $stockBookId
     * item-brand-size-color
     */
    public function setStockBookId($stockBookId)
    {
        $this->stockBookId = $stockBookId;
    }

    /**
     * @return float
     */
    public function getCmp()
    {
        return $this->cmp;
    }

    /**
     * @param float $cmp
     */
    public function setCmp($cmp)
    {
        $this->cmp = $cmp;
    }

    /**
     * @return float
     */
    public function getRemainingQuantity()
    {
        return $this->remainingQuantity;
    }

    /**
     * @param float $remainingQuantity
     */
    public function setRemainingQuantity( $remainingQuantity)
    {
        $this->remainingQuantity = $remainingQuantity;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    public function stockBookName()
    {
        $name = "";
        $item = $this->getItem()->getName();
        $brand = empty($this->getBrand()) ? '' :"- {$this->getBrand()->getName()}";
        $size = empty($this->getSize()) ? '' :"- {$this->getSize()->getName()}";
        $color = empty($this->getColor()) ? '' :"- {$this->getColor()->getName()}";
        $name = "{$item} {$brand}{$size}{$color}";
        //$name = "{$item} {$brand}{$size}{$color} {$this->getUom()}";
        return $name;
    }

    public function getUom()
    {
        $unit = empty($this->getItem()->getUnit()) ? '' :"{$this->getItem()->getUnit()->getName()}";
        return $unit;
    }

    public function getStockBookCategory()
    {
        $category = empty($this->getItem()->getCategory()) ? '' :"{$this->getItem()->getCategory()->getName()}";
        return $category;
    }

    /**
     * @return StockHistory
     */
    public function getStockHistories()
    {
        return $this->stockHistories;
    }

    /**
     * @return CmpPrice
     */
    public function getCmpPrices()
    {
        return $this->cmpPrices;
    }

    /**
     * @return \DateTime
     */
    public function getCmpDate()
    {
        return $this->cmpDate;
    }

    /**
     * @param \DateTime $cmpDate
     */
    public function setCmpDate($cmpDate)
    {
        $this->cmpDate = $cmpDate;
    }

    /**
     * @return bool
     */
    public function isDelete()
    {
        return $this->isDelete;
    }

    /**
     * @param bool $isDelete
     */
    public function setIsDelete($isDelete)
    {
        $this->isDelete = $isDelete;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }






}

