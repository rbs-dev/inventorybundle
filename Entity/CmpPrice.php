<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Application\Inventory;
use Core\UserBundle\Entity\User;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Setting\Bundle\ToolBundle\Entity\ProductUnit;
use Symfony\Component\Validator\Constraints\DateTime;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;


/**
 * StockItem
 *
 * @ORM\Table("inv_cmp_price")
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\CmpPriceRepository")
 */
class CmpPrice
{
    /**
     * @var integer
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook",inversedBy="cmpPrices")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $stockBook;

     /**
     * @ORM\ManyToOne(targetEntity="Terminalbd\ProcurementBundle\Entity\TenderWorkorder")
     * @ORM\JoinColumn(onDelete="CASCADE")
     **/
    protected  $workorder;


    /**
     * @Gedmo\Blameable(on="create")
     * @ORM\ManyToOne(targetEntity="App\Entity\User")
     **/
    private  $createdBy;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $prevCmp;


     /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $cmp;


    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable = true)
     */
    private $woPrice;


    /**
     * @var \DateTime
     * @ORM\Column(name="cmpDate", type="datetime", nullable=true)
     */
    private $cmpDate;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param mixed $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }

    /**
     * @return float
     */
    public function getPrevCmp()
    {
        return $this->prevCmp;
    }

    /**
     * @param float $prevCmp
     */
    public function setPrevCmp( $prevCmp)
    {
        $this->prevCmp = $prevCmp;
    }

    /**
     * @return float
     */
    public function getCmp()
    {
        return $this->cmp;
    }

    /**
     * @param float $cmp
     */
    public function setCmp( $cmp)
    {
        $this->cmp = $cmp;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }

    /**
     * @return mixed
     */
    public function getCreatedBy()
    {
        return $this->createdBy;
    }

    /**
     * @param mixed $createdBy
     */
    public function setCreatedBy($createdBy)
    {
        $this->createdBy = $createdBy;
    }

    /**
     * @return \DateTime
     */
    public function getCmpDate()
    {
        return $this->cmpDate;
    }

    /**
     * @param \DateTime $cmpDate
     */
    public function setCmpDate($cmpDate)
    {
        $this->cmpDate = $cmpDate;
    }

    /**
     * @return float
     */
    public function getWoPrice()
    {
        return $this->woPrice;
    }

    /**
     * @param float $woPrice
     */
    public function setWoPrice($woPrice)
    {
        $this->woPrice = $woPrice;
    }

    /**
     * @return mixed
     */
    public function getWorkorder()
    {
        return $this->workorder;
    }

    /**
     * @param mixed $workorder
     */
    public function setWorkorder($workorder)
    {
        $this->workorder = $workorder;
    }



}

