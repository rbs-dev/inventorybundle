<?php

namespace Terminalbd\InventoryBundle\Entity;

use App\Entity\Core\Customer;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Stock;

/**
 * @ORM\Entity(repositoryClass="Terminalbd\InventoryBundle\Repository\PurchaseItemRepository")
 * @ORM\Table(name="inv_purchase_item")
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseItem
{

    /**
     * @ORM\Id
     * @ORM\Column(name="id", type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     */
    protected $id;


    /**
     * @var Purchase
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Purchase",inversedBy="purchaseItems")
     * @ORM\JoinColumn(onDelete="CASCADE")
     */
    private $purchase;

    /**
     * @var Item
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\Item")
     */
    private $item;

    /**
     * @var Stock
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\Stock")
     */
    private $stock;

    /**
     * @var StockBook
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\InventoryBundle\Entity\StockBook")
     */
    private $stockBook;


    /**
     * @var ItemBrand
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemBrand")
     */
    private $brand;


    /**
     * @var ItemSize
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemSize")
     */
    private $size;


    /**
     * @var ItemColor
     *
     * @ORM\ManyToOne(targetEntity="Terminalbd\GenericBundle\Entity\ItemColor")
     */
    private $color;


    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $name;

    /**
     * @var string
     * @ORM\Column(type="string",nullable=true)
     */
    private $unit;


    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $quantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $minQuantity=0;

     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $pack=0;

    /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $subTotal=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $price=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $salesPrice=0;


     /**
     * @var float
     * @ORM\Column(type="float",nullable=true)
     */
    private $purchasePrice=0;


    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="create")
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var \DateTime
     * @Gedmo\Timestampable(on="update")
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;


    /**
     * Get id
     *
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }


    /**
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * @param string $unit
     */
    public function setUnit( $unit)
    {
        $this->unit = $unit;
    }

    /**
     * @return string
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param string $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @param bool $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return float
     */
    public function getSubTotal()
    {
        return $this->subTotal;
    }

    /**
     * @param float $subTotal
     */
    public function setSubTotal( $subTotal)
    {
        $this->subTotal = $subTotal;
    }

    /**
     * @return float
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * @param float $total
     */
    public function setTotal($total)
    {
        $this->total = $total;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @param \DateTime $created
     */
    public function setCreated($created)
    {
        $this->created = $created;
    }

    /**
     * @return \DateTime
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * @param \DateTime $updated
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;
    }


    /**
     * @return int
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;
    }


    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }
    
    /**
     * @return Item
     */
    public function getItem()
    {
        return $this->item;
    }

    /**
     * @param Item $item
     */
    public function setItem($item)
    {
        $this->item = $item;
    }

    /**
     * @return Stock
     */
    public function getStock()
    {
        return $this->stock;
    }

    /**
     * @param Stock $stock
     */
    public function setStock($stock)
    {
        $this->stock = $stock;
    }

    /**
     * @return Purchase
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * @param Purchase $purchase
     */
    public function setPurchase(Purchase $purchase)
    {
        $this->purchase = $purchase;
    }

    /**
     * @return float
     */
    public function getSalesPrice()
    {
        return $this->salesPrice;
    }

    /**
     * @param float $salesPrice
     */
    public function setSalesPrice(float $salesPrice)
    {
        $this->salesPrice = $salesPrice;
    }

    /**
     * @return float
     */
    public function getPurchasePrice()
    {
        return $this->purchasePrice;
    }

    /**
     * @param float $purchasePrice
     */
    public function setPurchasePrice(float $purchasePrice)
    {
        $this->purchasePrice = $purchasePrice;
    }

    /**
     * @return float
     */
    public function getMinQuantity(): float
    {
        return $this->minQuantity;
    }

    /**
     * @param float $minQuantity
     */
    public function setMinQuantity(float $minQuantity)
    {
        $this->minQuantity = $minQuantity;
    }

    /**
     * @return float
     */
    public function getPack()
    {
        return $this->pack;
    }

    /**
     * @param float $pack
     */
    public function setPack(float $pack)
    {
        $this->pack = $pack;
    }

    /**
     * @return ItemBrand
     */
    public function getBrand()
    {
        return $this->brand;
    }

    /**
     * @param ItemBrand $brand
     */
    public function setBrand(ItemBrand $brand)
    {
        $this->brand = $brand;
    }

    /**
     * @return ItemSize
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * @param ItemSize $size
     */
    public function setSize(ItemSize $size)
    {
        $this->size = $size;
    }

    /**
     * @return ItemColor
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param ItemColor $color
     */
    public function setColor(ItemColor $color)
    {
        $this->color = $color;
    }

    /**
     * @return StockBook
     */
    public function getStockBook()
    {
        return $this->stockBook;
    }

    /**
     * @param StockBook $stockBook
     */
    public function setStockBook($stockBook)
    {
        $this->stockBook = $stockBook;
    }




}
