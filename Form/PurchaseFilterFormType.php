<?php

namespace Terminalbd\InventoryBundle\Form;


use App\Entity\Admin\Terminal;
use App\Entity\Domain\Customer;
use App\Entity\Domain\Vendor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Purchase;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseFilterFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $builder



            ->add('startDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'datePicker','placeholder'=>"Enter Start Date"],
            ])
            ->add('endDate', DateType::class, [
                'widget' => 'single_text',
                'html5' => false,
                'required' => false,
                'mapped' => false,
                'attr' => ['class' => 'datePicker','placeholder'=>"Enter End Date"],
            ])
            ->add('vendor', EntityType::class, [
                'class' => Vendor::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a vendor name',
            ])
            ->add('purchaseNo', TextType::class, [
                'attr' => ['autofocus' => false,'placeholder'=>"Enter GRN"],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('filter', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm  purple-bg white-font'
                ]
            ])
            ->setMethod('GET')
            ->getForm();
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'terminal' => Terminal::class,
        ]);
    }
}
