<?php

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Admin\Terminal;
use App\Entity\Application\Inventory;
use App\Entity\Application\Procurement;
use App\Entity\Domain\Vendor;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\DataTransformer\DateTimeToStringTransformer;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\Purchase;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseIssueFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $terminal =  $options['terminal']->getId();
        $builder

            ->add('amount', NumberType::class, [
                'attr' => ['autofocus' => true,'class'=>'amount text-right'],
                 'required' => false,
            ])

            ->add('discount', HiddenType::class)

            ->add('discountCalculation', NumberType::class, [
                'attr' => ['autofocus' => true,'class'=>'amount text-right'],
                 'required' => false,
            ])

            ->add('percentType', ChoiceType::class, [
                'multiple' => false,
                'expanded' => false,
                'choices' => [
                    'Flat' => 'Flat',
                    'Percent' => 'Percent',
                ],
                'attr' => [
                    'class' => 'form-check-inline'
                ]
            ])

            ->add('content', TextareaType::class, [
                'attr' => ['autofocus' => true,'class'=>'textarea'],
                 'required' => false,
            ])

            ->add($builder->create('receiveDate', TextType::class, array(
                'label' => 'Receive Date',
                'required' => false,
                'attr' => array(
                    'class' => 'datePicker',
                    'autocomplete' => 'off',
                    'placeholder' => 'DD-MM-YY'
                ),
                'empty_data' => new \DateTime(),
            ))->addViewTransformer(new DateTimeToStringTransformer(null, null, 'd-m-Y')))

            ->add('vendor', EntityType::class, [
                'class' => Vendor::class,
                'required' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'companyName',
                'placeholder' => 'Choose a vendor name',
            ])

            ->add('file', FileType::class, [
                'required' => false,
                'attr'=>['class'=>'custom-file-input'],
                'constraints' => [
                new File([
                    'maxSize' => '1024k',
                    'mimeTypes' => [
                        'application/pdf',
                        'application/x-pdf',
                        'image/png',
                        'image/jpeg',
                        'image/jpg',
                        'image/gif'
                    ],
                    'mimeTypesMessage' => 'Please upload a valid PDF document',
                ])
            ],
            ])
            ->add('status',CheckboxType::class,[
                'required' => false,
                'attr' => [
                    'class' => 'checkboxToggle',
                    'data-toggle' => "toggle",
                    'data-style' => "slow",
                    'data-offstyle' => "warning",
                    'data-onstyle'=> "success",
                    'data-on' => "Enabled",
                    'data-off'=> "Disabled"
                ],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Purchase::class,
            'terminal' => Terminal::class,
        ]);
    }
}
