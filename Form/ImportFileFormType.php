<?php

namespace Terminalbd\InventoryBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\InventoryBundle\Entity\ImportFile;


class ImportFileFormType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $choiceValues = [
            'Category' => 'category',
            'Item' => 'item',
            'Vendor' => 'vendor',
            'Branch' => 'branch',
            'Designation' => 'designation',
            'Department' => 'department',
            'Employee' => 'employee',
        ];

        $builder

            ->add('type', ChoiceType::class, [
                'required' => true,
                'choices'  => $choiceValues
            ])

            ->add('file', FileType::class, [
                'required' => true,
                'help' => 'Accepts only excel file.',
                'attr'=>['class'=>'custom-file-input'],
                'constraints' => [
                    /*new File([
                        'maxSize' => '1024k',
                        'mimeTypesMessage' => 'Please upload a valid File',
                    ])*/
                ],
            ])
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => ImportFile::class,
        ]);
    }
}
