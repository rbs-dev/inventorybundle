<?php

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Application\GenericMaster;
use App\Entity\Core\Setting;
use App\Entity\Domain\Branch;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Category;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\GenericBundle\Entity\Particular;
use Terminalbd\GenericBundle\Repository\ItemRepository;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class InventoryFilterFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $terminal =  $options['config']->getTerminal();
        $builder

            ->add('productGroup', EntityType::class, [
                'class' => Particular::class,
                'required' => false,
                'query_builder' => function (EntityRepository $er) use($config){
                    return $er->createQueryBuilder('e')
                        ->join("e.particularType","st")
                        ->where("st.slug ='product-group'")
                        ->andWhere("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'span12 select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a  product group',
            ])

            ->add('itemMode',ChoiceType::class,
                array(
                    'attr'=>array('class'=>''),
                    'required'    => false,
                    'mapped' => false,
                    'choices'  => [
                        'OPEX' => 'OPEX',
                        'CAPEX' => 'CAPEX',
                        'Expense' => 'Expense',
                        'Kanban' => 'Kanban',
                        'Others' => 'Others',
                    ],
                    'placeholder' => 'Choose a  item mode',
                )
            )

            ->add('wearhouse', EntityType::class, [
                'class' => Branch::class,
                'required' => true,
                'mapped' => false,
                'group_by'  => 'parent.name',
                'choice_translation_domain' => true,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->where('e.status =1')
                        ->andWhere("e.branchType ='sub-branch'")
                        ->andWhere("e.terminal ='{$terminal}'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a waer house',
            ])

            ->add('department', EntityType::class, [
                'class' => Setting::class,
                'required' => false,
                'mapped' => false,
                'query_builder' => function (EntityRepository $er) use($terminal) {
                    return $er->createQueryBuilder('e')
                        ->join("e.settingType",'t')
                        ->where("e.status =1")
                        ->andWhere("e.terminal ={$terminal}")
                        ->andWhere("t.slug ='department'")
                        ->orderBy('e.name', 'ASC');
                },
                'attr'=>['class'=>'select2'],
                'choice_label' => 'name',
                'placeholder' => 'Choose a GL Department',
            ])

            ->add('category', EntityType::class, [
                'class' => Category::class,
                'attr'=>['class'=>'select2'],
                'required' => false,
                'placeholder' => 'Choose a category',
                'choice_label' => 'nestedLabel',
                'choices'   => $options['categoryRepo']->getFlatCategoryTree($options['config'])
            ])

            /*->add('item', EntityType::class, [
                'class' => Item::class,
                'multiple' => false,
                'required' => false,
                'mapped' => false,
                'choice_label'  => 'name',
                'attr'=>['class'=>'select2'],
                'placeholder' => 'Choose a item name',
                'query_builder' => function(EntityRepository $er)  use($config){
                    return $er->createQueryBuilder('e')
                        ->join('e.config','b')
                        ->where('b.id = :config')->setParameter('config', $config)
                        ->orderBy('e.name', 'ASC');
                },
            ])*/

            ->add('size', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => false,
                'mapped'      => false,
                'class' => ItemSize::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose a size',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('color', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => false,
                'mapped'      => false,
                'class' => ItemColor::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose a color',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('brand', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => false,
                'mapped'      => false,
                'class' => ItemBrand::class,
                'choice_label' => 'name',
                'placeholder' => 'Choose a brand',
                'attr'=>array('class'=>'select2'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =1")
                        ->andWhere("e.config ={$config}")
                        ->orderBy('e.name', 'ASC');
                },
            ))

            ->add('name', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Enter Item name'],
                'required' => false,
                'mapped' => false,
            ])
            
            ->add('itemCode', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Enter Item Code'],
                'required' => false,
                'mapped' => false,
            ])
            ->add('keyword', TextType::class, [
                'attr' => ['autofocus' => true,'placeholder' => 'Enter keyword'],
                'required' => false,
                'mapped' => false,
            ])
            ->add('filter', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-sm  purple-bg white-font'
                ]
            ])
            ->setMethod('GET')
            ->getForm()
        ;
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'config' => GenericMaster::class,
            'categoryRepo' => CategoryRepository::class,
        ]);
    }
}
