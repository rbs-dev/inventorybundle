<?php

namespace Terminalbd\InventoryBundle\Form;

use App\Entity\Application\GenericMaster;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Contracts\Translation\TranslatorInterface;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;


/**
 * Defines the form used to create and manipulate blog posts.
 *
 * @author Md Shafiqul Islam <shafiqabs@gmail.com>
 */
class PurchaseGarmentsItemFormType extends AbstractType
{


    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $config =  $options['config']->getId();
        $builder


            ->add('item', EntityType::class, [
                'class' => Item::class,
                'multiple' => false,
                'group_by'  => 'category.name',
                'choice_label'  => 'name',
                'attr'=>['class'=>'select2 itemName'],
                'placeholder' => 'Choose a item name',
                'choice_translation_domain' => true,
                'query_builder' => function(EntityRepository $er)  use($config){
                    return $er->createQueryBuilder('e')
                        ->join('e.config','b')
                        ->where('b.id = :config')->setParameter('config', $config)
                        ->orderBy('e.name', 'ASC');
                },
            ])
            ->add('size', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => false,
                'class' => ItemSize::class,
                'choice_label' => 'name',
                'placeholder'      => 'Choose a size',
                'attr'=>array('class'=>'size'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =-1")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('color', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => false,
                'placeholder'      => 'Choose a color',
                'class' => ItemColor::class,
                'choice_label' => 'name',
                'attr'=>array('class'=>'color'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =-1")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('brand', EntityType::class, array(
                'required'      => false,
                'expanded'      => false,
                'multiple'      => false,
                'class' => ItemBrand::class,
                'choice_label' => 'name',
                'placeholder'      => 'Choose a brand',
                'attr'=>array('class'=>'brand'),
                'query_builder' => function(EntityRepository $er)use($config){
                    return $er->createQueryBuilder('e')
                        ->where("e.status =-1")
                        ->orderBy('e.name', 'ASC');
                },
            ))
            ->add('quantity', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'number-input'],
                'required' => true,
            ])
            ->add('price', NumberType::class, [
                'attr' => ['autofocus' => true,'class' => 'number-input'],
                'required' => true,
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => PurchaseItem::class,
            'config' => GenericMaster::class,
        ]);
    }
}
