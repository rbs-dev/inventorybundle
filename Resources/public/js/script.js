function jqueryTemporaryLoad() {

    $('.amount').change(function(){
        this.value = parseFloat(this.value).toFixed(2);
    });

    function financial(val) {
        return Number.parseFloat(val).toFixed(2);
    }

    $('.cmpDatePicker').datepicker({
        format: "dd-mm-yyyy",
        todayHighlight:true,
        changeMonth: true,
        changeYear: true,
        autoclose: true
    });

    $('#item').select2('open');
    function jsonResult(response){
        obj = JSON.parse(response);
        $('#invoiceItem').html(obj['invoiceItem']);
        $('#subTotal').html(financial(obj['subTotal']));
        $('#total').html(financial(obj['total']));
    }


    $('form#approveForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#approveForm').attr( 'action' ),
            type        : $('form#approveForm').attr( 'method' ),
            data        : new FormData($('form#approveForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                location.reload();
            }
        });
    });

    $(document).on('change', '.itemName', function() {

        var id = $(this).val();
        $.ajax({
            url: Routing.generate('inv_category_attribute'),
            type: 'POST',
            data:'id='+ id,
            success: function(response) {
                  obj = JSON.parse(response);
                  $(".brand").html(obj['brand']);
                  $(".size").html(obj['size']);
                  $(".color").html(obj['color']);
            }
        })

    });

    $('form#itemForm').submit(function(e){

        e.preventDefault();
        $.ajax({
            url         : $('form#itemForm').attr( 'action' ),
            type        : $('form#itemForm').attr( 'method' ),
            data        : new FormData($('form#itemForm')[0]),
            processData : false,
            contentType : false,
            success: function(response){
                $('#stockBookItem').html(response);
                $("#item").select2('open').prop('selectedIndex',0);
                $('form#itemForm')[0].reset();
            }
        });
    });

    $(document).on('change', '.quantity', function() {

        var id = $(this).attr('data-id');
        var quantity = parseFloat($('#quantity-'+id).val());
        var salesPrice = parseFloat($('#price-'+id).val());
        var subTotal  = (quantity * salesPrice);
        $("#subTotal-"+id).html(financial(subTotal));
        $.ajax({
            url: Routing.generate('inv_purchase_item_update'),
            type: 'POST',
            data:'item='+ id +'&quantity='+quantity,
            success: function(response) {
                setTimeout(jsonResult(response),100);
            }

        })
    });

    $(document).on('change', '.priceUpdate', function() {
        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        var price = parseFloat($('#cmp-'+id).val());
        var cmpdate = $('#cmpDate-'+id).val();
        $("#price-"+id).html(price);
        $.ajax({
            url: url,
            type: 'POST',
            data:'price='+ price +'&cmpdate=' + cmpdate,
            success: function(response) {
                $('#cmpDate-'+id).html(response);
            }
        })
    });

    $(document).on('click',".itemRemove", function (event) {

        var id = $(this).attr('data-id');
        var url = $(this).attr('data-action');
        $.MessageBox({
            buttonFail  : "No",
            buttonDone  : "Yes",
            message     : "Are you sure want to delete this record?"
        }).done(function(){
            $.get(url, function( response ) {
                $.get(url, function( response ) {
                    if(response === 'invalid'){
                        location.reload();
                    }else{
                        $(event.target).closest('tr').hide();
                        setTimeout(jsonResult(response),100);
                    }
                });

            });
        });
    });

    $(".autocomplete2Item").select2({
        ajax: {
            url: Routing.generate('inv_stock_item_autocomplete'),
            data: function (params, page) {
                return {
                    q: params,
                    page_limit: 100
                };
            },
            processResults: function (data) {
                return {
                    results: data
                };
            }
        },
        placeholder: 'Search for a add stock item',
        minimumInputLength: 1,
    });

    $(document).on('change', '.autocomplete2Item', function () {
        var employee = $(this).val();
        alert(employee);
    });
}






