$(document).ready(function () {

    var dataTable = $('#datatable').DataTable( {

        "processing": true,
        "serverSide": true,
        "bStateSave": true, // save datatable state(pagination, sort, etc) in cookie.
        'searching': true,
        "lengthMenu": [
            [20, 50, 100, 150, -1],
            [20, 50, 100, 150, "All"] // change per page values here
        ],
        "initComplete": function() {
            $('.status').bootstrapToggle()
        },
        "pageLength": 20, // default record count per page
        "ajax": {
            "type"   : "POST",
            "url": Routing.generate('inv_stock_data_table'),
            'data': function(data){

            }
        },
        "aoColumnDefs" : [
            {"aTargets" : [6], "sClass":  "text-center"}
        ],
        "order": [
            [1, "asc"]
        ],// set first column as a default sort by asc
        "columnDefs": [ {
            "targets": 6,
            "orderable": false
        },
            {
                "targets": 0,
                "orderable": false
            }],

    });

    $('#name').keyup(function(){
        dataTable.draw();
    });
    $('#category').keyup(function(){
        dataTable.draw();
    });




});

