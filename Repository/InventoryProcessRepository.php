<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\InventoryProcess;
use Terminalbd\InventoryBundle\Entity\Purchase;



/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class InventoryProcessRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, InventoryProcess::class);
    }

    public function insertProcess(User $user ,Purchase $requisition)
    {
        $em = $this->_em;
        $entity = new InventoryProcess();
        $entity->setConfig($requisition->getConfig());
        $entity->setRequisition($requisition);
        $entity->setCreatedBy($user);
        $entity->setProcess($requisition->getProcess());
        $entity->setComment($requisition->getComment());
        $em->persist($entity);
        $em->flush();

    }

    public function insertApproveProcess(Purchase $purchase)
    {
        $em = $this->_em;
        $entity = new InventoryProcess();
        $entity->setConfig($purchase->getConfig());
        $entity->setPurchase($purchase);
        $entity->setCreatedBy($user);
        $entity->setProcess($purchase->getProcess());
        $entity->setComment($purchase->getComment());
        $em->persist($entity);
        $em->flush();

    }

}
