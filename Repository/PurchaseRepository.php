<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\Admin\AppModule;
use App\Entity\Domain\ModuleProcess;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\InventoryProcess;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\ProcurementProcess;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;
use Terminalbd\ProcurementBundle\Entity\RequisitionItemHistory;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class PurchaseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Purchase::class);
    }

    protected function handleSearchBetween($qb,$form)
    {

        if(isset($form['purchase_filter_form'])){
            $data = $form['purchase_filter_form'];
            $startDate = isset($data['startDate'])? $data['startDate'] :'';
            $startDate = isset($data['endDate'])? $data['endDate'] :'';
            $vendor = isset($data['vendor'])? $data['vendor'] :'';
            $purchaseNo = !empty($data['purchaseNo'])? $data['purchaseNo'] :'';
            if(!empty($vendor)){
                $qb->andWhere('v.id = :vendor')->setParameter('vendor',$vendor);
            }
            if(!empty($purchaseNo)){
                $qb->andWhere($qb->expr()->like("e.purchaseNo", "'%$purchaseNo%'"));
            }
            if (!empty($startDate) ) {
                $datetime = new \DateTime($startDate);
                $startDate = $datetime->format('Y-m-d 00:00:00');
                $qb->andWhere("e.created >= :startDate");
                $qb->setParameter('startDate', $startDate);
            }

            if (!empty($endDate)) {
                $datetime = new \DateTime($endDate);
                $endDate = $datetime->format('Y-m-d 23:59:59');
                $qb->andWhere("e.created <= :endDate");
                $qb->setParameter('endDate', $endDate);
            }


        }


    }

    /**
     * @return Purchase[]
     */
    public function findBySearchQuery( $config, $data = "" ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.created';
        $direction = isset($data['direction'])? $data['direction'] :'desc';
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.id','e.purchaseNo as purchaseNo','e.created as created','e.subTotal as subTotal','e.discount as discount','e.amount','e.total as total','e.process as process');
        $qb->addSelect('v.name as vendor');
        $qb->addSelect('e.path as path');
        $qb->leftJoin('e.vendor','v');
        $qb->where('e.config = :config')->setParameter('config',$config);
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("{$sort}",$direction);
        $result = $qb->getQuery()->getArrayResult();
        return $result;
    }

    public function generateStoreRequisition(Purchase $requisition , $process = "")
    {
        $em =$this->_em;

        $module = $em->getRepository(AppModule::class)->findOneBy(array('slug' => $process));
        $terminal = $requisition->getCreatedBy()->getTerminal()->getId();

        /* @var $moduleProcess ModuleProcess */
        $moduleProcess = $em->getRepository(ModuleProcess::class)->findOneBy(array('terminal' =>$terminal,'module' => $module));

        foreach ($moduleProcess->getModuleProcessItems() as $row){
            $exist = $em->getRepository(ModuleProcess::class)->findOneBy(array('requisition'=>$requisition,'moduleProcessItem'=>$row));
            if(empty($exist)){
                $entity = new InventoryProcess();
                $entity->setPurchase($requisition);
                $entity->setModuleProcessItem($row);
                $entity->setProcess($requisition->getProcess());
                $em->persist($entity);
                $em->flush();
            }
        }


    }

    public function requisitionItemHistory(Purchase $purchase)
    {
        $em =$this->_em;
        /* @var $item RequisitionItem */

        if(!empty($purchase->getPurchaseItems())){

            foreach ($purchase->getPurchaseItems() as $item){

                $entity = new StockHistory();
                $entity->setPurchaseItem($item->getId());
                $entity->setPurchaseQuantity($item->getQuantity());
                $entity->setQuantity($item->getQuantity());
                $em->persist($entity);
                $em->flush();

            }
        }
    }


}
