<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class StockHistoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockHistory::class);
    }


    public function getItemOpeningQuantity(Stock $item)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('(COALESCE(SUM(e.quantity),0) AS openingQuantity');
        $qb->where("e.item = :item")->setParameter('item', $item->getId());
        $result = $qb->getQuery()->getSingleResult();
        $openingQuantity = $result['openingQuantity'];
        return $openingQuantity;

    }

    public function  getLastPurchaseItemInformation($stock)
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('e.stockIn as purchaseQuantity','e.price as price','e.updated as updated');
        $qb->where('e.stockBook = :stock')->setParameter('stock',$stock);
        $qb->andWhere("e.process = 'receive'");
        $qb->setMaxResults(1);
        $qb->orderBy('e.id',"DESC");
        $result = $qb->getQuery()->getOneOrNullResult();
        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public function stockReceiveSockBookItem($wearhouse,$stock,$process = 'receive')
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.stockIn) as quantity');
        $qb->where('e.stockBook = :stock')->setParameter('stock',$stock);
        $qb->andWhere('e.wearhouse = :wearhouse')->setParameter('wearhouse',$wearhouse);
        $qb->andWhere('e.process = :process')->setParameter('process',$process);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function stockIssueStockBookItem($wearhouse,$stock,$process = "issue")
    {
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.stockOut) as quantity');
        $qb->where('e.stockBook = :stock')->setParameter('stock',$stock);
        $qb->andWhere('e.wearhouse = :wearhouse')->setParameter('wearhouse',$wearhouse);
        $qb->andWhere('e.process = :process')->setParameter('process',$process);
        $result = $qb->getQuery()->getSingleScalarResult();
        return $result;
    }

    public function updateItemClosingQuantity(RestaurantStockHistory $stock)
    {
        $em = $this->_em;
        $closingQnt = ($stock->getOpeningQuantity() + $stock->getQuantity());
        $stock->setClosingQuantity($closingQnt);
        $em->persist($stock);
        $em->flush();

    }

    public function processWorkorderStockQuantity(TenderWorkorderReceiveItem $receiveItem)
    {

        $em = $this->_em;
        $item = $receiveItem->getStockBook();
        $entity = new StockHistory();
        $entity->setQuantity($receiveItem->getQuantity());
        $entity->setStockIn($receiveItem->getQuantity());
        if($receiveItem->getWorkorderReceive()->getMode() !== "Direct" and $receiveItem->getTenderWorkorderItem()->getWearhouse()){
            $entity->setWearHouse($receiveItem->getTenderWorkorderItem()->getWearhouse());
            $entity->setBranch($entity->getWearHouse()->getParent());
        }else{
            $entity->setWearHouse($receiveItem->getWorkorderReceive()->getWearhouse());
            $entity->setBranch($receiveItem->getWorkorderReceive()->getWearhouse()->getParent());
        }
        $entity->setStockBook($item);
        $entity->setStock($item->getStock());
        $entity->setItem($item->getItem());
        if($item->getCategory()){
            $entity->setCategory($item->getCategory());
        }
        if($item->getBrand()){
            $entity->setBrand($item->getBrand());
        }
        if($item->getSize()){
            $entity->setSize($item->getSize());
        }
        if($item->getColor()){
            $entity->setColor($item->getColor());
        }
        $entity->setProcess('receive');
        $entity->setPrice($receiveItem->getPrice());
        $entity->setConfig($item->getConfig());
        $em->persist($entity);
        $em->flush();
    }

    public function processDirectStockReceiveQuantity(TenderWorkorderReceiveItem $receiveItem)
    {

        $em = $this->_em;
        $item = $receiveItem->getStockBook();
        $entity = new StockHistory();
        $entity->setQuantity($receiveItem->getQuantity());
        $entity->setStockIn($receiveItem->getQuantity());
        if($receiveItem->getWorkorderReceive()->getWearhouse()){
            $entity->setWearHouse($receiveItem->getWorkorderReceive()->getWearhouse());
            $entity->setBranch($entity->getWearHouse()->getParent());
        }
        $entity->setStockBook($item);
        $entity->setStock($item->getStock());
        $entity->setItem($item->getItem());
        if($item->getCategory()){
            $entity->setCategory($item->getCategory());
        }
        if($item->getBrand()){
            $entity->setBrand($item->getBrand());
        }
        if($item->getSize()){
            $entity->setSize($item->getSize());
        }
        if($item->getColor()){
            $entity->setColor($item->getColor());
        }
        $entity->setProcess('receive');
        $entity->setPrice($receiveItem->getPrice());
        $entity->setConfig($item->getConfig());
        $em->persist($entity);
        $em->flush();
    }

    public function processBankWorkorderStockQuantity(TenderWorkorderReceiveItem $receiveItem)
    {

        $em = $this->_em;
        $item = $receiveItem->getStock();
        $entity = new StockHistory();
        $entity->setQuantity($receiveItem->getQuantity());
        $entity->setStockIn($receiveItem->getQuantity());
        if($receiveItem->getWorkorderReceive()->getWorkorder()->getShipTo()){
            $entity->setWearHouse($receiveItem->getWorkorderReceive()->getWorkorder()->getShipTo());
            $entity->setBranch($entity->getWearHouse()->getParent());
        }
        $entity->setStock($receiveItem->getStock());
        $entity->setItem($item->getItem());
        //$entity->setWearHouse($receiveItem->getWorkorderReceive()->getWorkorder()->getWearhouse());
        if($item->getCategory()){
            $entity->setCategory($item->getCategory());
        }
        $entity->setProcess('receive');
        $entity->setPrice($receiveItem->getPrice());
        $entity->setConfig($item->getConfig());
        $em->persist($entity);
        $em->flush();
    }
    
    public function processOrderIssueQuantity(RequisitionOrderItem $receiveItem)
    {

        $em = $this->_em;
        $item = $receiveItem->getStockBook();
        $entity = new StockHistory();
        $entity->setQuantity($receiveItem->getQuantity());
        $entity->setStockOut($receiveItem->getQuantity());
        if($receiveItem->getRequisitionOrder()->getRequisitionIssue()->getWearhouse()){
            $entity->setWearHouse($receiveItem->getRequisitionOrder()->getRequisitionIssue()->getWearhouse());
            $entity->setBranch($entity->getWearHouse()->getParent());
        }
        $entity->setStockBook($item);
        $entity->setStock($item->getStock());
        $entity->setItem($item->getItem());
        if($item->getCategory()){
            $entity->setCategory($item->getCategory());
        }
        if($item->getBrand()){
            $entity->setBrand($item->getBrand());
        }
        if($item->getSize()){
            $entity->setSize($item->getSize());
        }
        if($item->getColor()){
            $entity->setColor($item->getColor());
        }
        $entity->setProcess('issue');
        $entity->setPrice($receiveItem->getPrice());
        $entity->setConfig($item->getConfig());
        $em->persist($entity);
        $em->flush();
    }

    public function processStockQuantity($item, $fieldName = '')
    {

        $em = $this->_em;

       // $openingQnt = $this->getItemOpeningQuantity($item->getStock());

        $entity = new StockHistory();

        if ($fieldName == 'purchase') {

            /* @var $item PurchaseItem */

            $exist = $this->findOneBy(array('purchaseItem' => $item));
            if ($exist) {
                $entity = $exist;
            }
            $entity->setQuantity($item->getQuantity());
            $entity->setPurchaseQuantity($item->getQuantity());
            $entity->setStock($item->getStock());
            $entity->setItem($item->getItem());
            if($item->getItem()->getCategory()){
                $entity->setCategory($item->getItem()->getCategory());
            }
            if($item->getPurchase()->getVendor()){
                $entity->setVendor($item->getPurchase()->getVendor());
            }
            if($item->getBrand()){
                $entity->setBrand($item->getBrand());
            }
            if($item->getSize()){
                $entity->setSize($item->getSize());
            }
            if($item->getColor()){
                $entity->setColor($item->getColor());
            }
            $entity->setPurchaseItem($item->getId());
            $entity->setProcess('purchase');
            $entity->setPrice($item->getPrice());


        } elseif ($fieldName == 'purchase-return') {

            /* @var $item BusinessPurchaseReturnItem */

            $entity->setQuantity("-{$item->getQuantity()}");
            $entity->setPurchaseReturnQuantity($item->getQuantity());
            $entity->setItem($item->getParticular());
            $entity->setPurchaseReturnItem($item);
            $entity->setProcess('purchase-return');

        } elseif ($fieldName == 'sales') {

            /* @var $item InvoiceParticular */

            $entity->setQuantity("-{$item->getQuantity()}");
            $entity->setSalesQuantity($item->getQuantity());
            $entity->setItem($item->getParticular());
            $entity->setSalesItem($item);
            $entity->setProcess('sales');

        } elseif ($fieldName == 'sales-return') {

            /* @var $item BusinessInvoiceReturn */

            $entity->setQuantity($item->getQuantity());
            $entity->setSalesQuantity($item->getQuantity());
            $entity->setItem($item->getParticular());
            $entity->setSalesItem($item);
            $entity->setProcess('sales-return');

        } elseif ($fieldName == 'damage') {

            /* @var $item RestaurantDamage */

            $entity->setQuantity("-{$item->getQuantity()}");
            $entity->setDamageQuantity($item->getQuantity());
            $entity->setItem($item->getParticular());
            $entity->setDamageItem($item);
            $entity->setProcess('sales-return');

        } elseif ($fieldName == 'production') {

            /* @var $item RestaurantDamage */

            $entity->setQuantity("-{$item->getQuantity()}");
            $entity->setProductionQuantity($item->getQuantity());
            $entity->setItem($item->getParticular());
            $entity->setProductionExpense($item);
            $entity->setProcess('production');

        }
/*        if ($openingQnt) {
            $entity->setOpeningQuantity(floatval($openingQnt));
        } else {
            $entity->setOpeningQuantity(0);
        }
        $closingQuantity = $entity->getQuantity() + $entity->getOpeningQuantity();
        $entity->setClosingQuantity(floatval($closingQuantity));*/
        $entity->setConfig($item->getPurchase()->getConfig());
        $em->persist($entity);
        $em->flush();

    }

    public function processInsertPurchaseItem(Purchase $entity)
    {

        $em = $this->_em;

        /** @var $item PurchaseItem */

        if ($entity->getPurchaseItems()) {

            foreach ($entity->getPurchaseItems() as $item) {
                $em->createQuery("DELETE TerminalbdInventoryBundle:StockHistory e WHERE e.purchaseItem = '{$item->getId()}'")->execute();
                $this->processStockQuantity($item, "purchase");
            }
        }
    }


    public function processInsertDamageItem(RestaurantDamage $entity)
    {

        $em = $this->_em;
        $em->createQuery("DELETE RestaurantBundle:RestaurantStockHistory e WHERE e.damage = '{$entity->getId()}'")->execute();
        $this->processStockQuantity($entity, "damage");
    }

    public function processReversePurchaseItem(Purchase $entity)
    {

        $em = $this->_em;

        /** @var $item Purchase */

        if ($entity->getPurchaseItems()) {
            foreach ($entity->getPurchaseItems() as $item) {
                $em->createQuery("DELETE RestaurantBundle:RestaurantStockHistory e WHERE e.purchaseItem = '{$item->getId()}'")->execute();
            }
        }
    }

    public function processInsertPurchaseReturnItem(BusinessPurchaseReturn $entity)
    {

        $em = $this->_em;

        /** @var $item BusinessPurchaseReturnItem */

        /* if($entity->getBusinessPurchaseReturnItems()){

             foreach($entity->getBusinessPurchaseReturnItems() as $item ){
                 $em->createQuery("DELETE BusinessBundle:RestaurantStockHistory e WHERE e.purchaseReturnItem = '{$item->getId()}'")->execute();
                 if($item->getQuantity() > 0){
                     $this->processStockQuantity($item,"purchase-return");
                 }
             }
         }*/
    }

    public function processInsertSalesItem(Invoice $entity)
    {

        $em = $this->_em;

        /** @var $item InvoiceParticular */

        if ($entity->getInvoiceParticulars()) {

            foreach ($entity->getInvoiceParticulars() as $item) {
                $em->createQuery("DELETE RestaurantBundle:RestaurantStockHistory e WHERE e.salesItem = '{$item->getId()}'")->execute();
                if ($item->getParticular()->getService()->getSlug() == 'stockable' and $item->getQuantity() > 0) {
                    $this->processStockQuantity($item, "sales");
                }
            }
        }
    }

    public function processInsertProductionItem(ProductionExpense $entity)
    {

        $em = $this->_em;
        $em->createQuery("DELETE RestaurantBundle:RestaurantStockHistory e WHERE e.productionExpense = '{$entity->getId()}'")->execute();
        $this->processStockQuantity($entity, "production");

    }

    public function processReverseSalesItem(Invoice $entity)
    {

        $em = $this->_em;
        /** @var $item InvoiceParticular */
        if ($entity->getInvoiceParticulars()) {
            foreach ($entity->getInvoiceParticulars() as $item) {
                $em->createQuery("DELETE RestaurantBundle:RestaurantStockHistory e WHERE e.salesItem = '{$item->getId()}'")->execute();
            }
        }
    }


    public function processInsertSalesReturnItem(BusinessInvoice $entity)
    {

        $em = $this->_em;

        /** @var $item BusinessInvoiceReturn */

        /*if($entity->getBusinessInvoiceParticulars()){

            foreach($entity->getBusinessInvoiceParticulars() as $item ){
                $em->createQuery("DELETE BusinessBundle:RestaurantStockHistory e WHERE e.salesReturnItem = '{$item->getId()}'")->execute();
                if($item->getQuantity() > 0){
                    $this->processStockQuantity($item,"sales-return");
                }
            }
        }*/
    }


    public function openingDailyQuantity(RestaurantConfig $config, $data)
    {

        $item = isset($data['name']) ? $data['name'] : '';
        if (isset($data['startDate'])) {
            $date = new \DateTime($data['startDate']);
        } else {
            $date = new \DateTime();
        }
        $date->add(\DateInterval::createFromDateString('yesterday'));
        $tillDate = $date->format('Y-m-d 23:59:59');
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.quantity) as openingQnt');
        $qb->join("e.item", 'i');
        $qb->where("i.name = :name")->setParameter('name', $item);
        $qb->andWhere("e.created <= :created")->setParameter('created', $tillDate);
        $lastCode = $qb->getQuery()->getOneOrNullResult()['openingQnt'];
        if (empty($lastCode)) {
            return '';
        }
        return $lastCode;

    }

    public function monthlyStockLedger(RestaurantConfig $config, $data)
    {
        $config = $config->getId();
        $compare = new \DateTime();
        $month = $compare->format('F');
        $year = $compare->format('Y');
        $month = isset($data['month']) ? $data['month'] : $month;
        $year = isset($data['year']) ? $data['year'] : $year;
        $item = isset($data['name']) ? $data['name'] : '';
        $sql = "SELECT DATE_FORMAT(e.created,'%d-%m-%Y') as date ,COALESCE(SUM(e.purchaseQuantity),0) as purchaseQuantity,COALESCE(SUM(e.purchaseReturnQuantity),0) as purchaseReturnQuantity,COALESCE(SUM(e.salesQuantity),0) as salesQuantity,COALESCE(SUM(e.salesReturnQuantity),0) as salesReturnQuantity,COALESCE(SUM(e.damageQuantity),0) as damageQuantity
                FROM restaurant_stock_history as e
                JOIN restaurant_particular ON e.item_id = particular.id
                WHERE e.restaurantConfig_id = :config AND particular.name = :item  AND MONTHNAME(e.created) =:month AND YEAR(e.created) =:year
                GROUP BY date";
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $stmt->bindValue('config', $config);
        $stmt->bindValue('item', "{$item}");
        $stmt->bindValue('month', $month);
        $stmt->bindValue('year', $year);
        $stmt->execute();
        $results = $stmt->fetchAll();
        $arrays = [];
        foreach ($results as $result) {
            $arrays[$result['date']] = $result;
        }
        return $arrays;
    }

}