<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\User;
use Cassandra\Date;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\CmpPrice;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class CmpPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CmpPrice::class);
    }
    public function insertCmpPrice(User $user ,StockBook $stock,$price,$cmpdate)
    {
        $em = $this->_em;
        $entity = new CmpPrice();
        $entity->setCreatedBy($user);
        $entity->setStockBook($stock);
        $entity->setCmp($stock->getCmp());
        $entity->setPrevCmp($stock->getCmp());
        $entity->setCmp($price);
        $data = new \DateTime();
        $entity->setCmpDate($data);
        $em->persist($entity);
        $em->flush();
    }
    public function insertWoPrice(TenderWorkorder $workorder ,StockBook $stock,$price)
    {
        $em = $this->_em;
        $entity = new CmpPrice();
        $entity->setCreatedBy($workorder->getCreatedBy());
        $entity->setWorkorder($workorder);
        $entity->setStockBook($stock);
        $entity->setWoPrice($price);
        $entity->setPrevCmp($stock->getPrice());
        $entity->setCmp($price);
        $entity->setCmpDate($workorder->getUpdated());
        $em->persist($entity);
        $em->flush();
    }

}
