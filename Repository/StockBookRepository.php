<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\Application\GenericMaster;
use App\Entity\Application\Inventory;
use Appstore\Bundle\MedicineBundle\Entity\MedicineConfig;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\CmpPrice;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorder;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class StockBookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockBook::class);
    }


    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['inventory_filter_form'])) {
            $data = $form['inventory_filter_form'];
            $item               = isset($data['item'])? $data['item'] :'';
            $itemCode           = isset($data['itemCode'])? $data['itemCode'] :'';
            $category           = isset($data['category'])? $data['category'] :'';
            $brand              = isset($data['brand'])? $data['brand'] :'';
            $size               = isset($data['size'])? $data['size'] :'';
            $color              = isset($data['color'])? $data['color'] :'';
            $keyword            = isset($data['keyword'])? $data['keyword'] :'';
            $itemMode           = isset($data['itemMode'])? $data['itemMode'] :'';
            $item               = isset($data['name'])? $data['name'] :'';
            $department               = isset($data['department'])? $data['department'] :'';
            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($department)) {
                $qb->andWhere("dp.id = :did")->setParameter('did', $department);
            }
            if (!empty($item)) {
                $qb->andWhere("item.id = :pid")->setParameter('pid', $item);
            }
            if (!empty($itemMode)) {
                $qb->andWhere("item.itemMode = :itemMode")->setParameter('itemMode', $itemMode);
            }
            if (!empty($item)) {
                $qb->andWhere('item.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$item.'%');
            }
            if (!empty($brand)) {
                $qb->andWhere("brand.id = :bid")->setParameter('bid', $brand);
            }
            if (!empty($size)) {
                $qb->andWhere("size.id = :sid")->setParameter('sid', $size);
            }
            if (!empty($color)) {
                $qb->andWhere("color.id = :coid")->setParameter('coid', $color);
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($keyword)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm OR category.name LIKE :searchTerm OR item.name LIKE :searchTerm OR brand.name LIKE :searchTerm OR size.name LIKE :searchTerm  OR color.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($keyword).'%');
            }
        }
    }

    public function findBySearchQuery( $config, $data )
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';

        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.item','item');
        $qb->leftJoin('e.brand','b');
        $qb->leftJoin('e.category','category');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('gl.department','dp');
        $qb->leftJoin('e.brand','brand');
        $qb->leftJoin('e.size','size');
        $qb->leftJoin('e.color','color');
        $qb->leftJoin('item.unit','unit');
        $qb->select("e.id as id","e.remainingQuantity as remainingQuantity","e.stockIn as stockIn","e.stockOut as stockOut","e.price as price","e.cmp as cmp","e.minimumQuantity as minimumQuantity","e.reorderQuantity as reorderQuantity,e.updated as updated");
        $qb->addSelect("item.itemCode as itemCode","item.name as name");
        $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
        $qb->addSelect("unit.name as unitName");
        $qb->addSelect("category.name as categoryName");
        $qb->addSelect("size.name as sizeName");
        $qb->addSelect("color.name as colorName");
        $qb->addSelect("brand.name as brandName");
        $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
        $qb->addSelect("dp.name as department");
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere("e.isDelete = 0");
        $qb->andWhere("e.status = 1");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("item.name",'ASC');
        $result = $qb->getQuery();
        return  $result;
    }

    public function findByKanbanQuery( $config, $data ): array
    {

        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        $qb = $this->createQueryBuilder('e');
        $qb->leftJoin('e.item','item');
        $qb->leftJoin('e.brand','b');
        $qb->leftJoin('e.category','category');
        $qb->leftJoin('category.generalLedger','gl');
        $qb->leftJoin('e.brand','brand');
        $qb->leftJoin('e.size','size');
        $qb->leftJoin('e.color','color');
        $qb->leftJoin('item.unit','unit');
        $qb->select("e.id as id","e.remainingQuantity as remainingQuantity","e.stockIn as stockIn","e.stockOut as stockOut","e.price as price","e.cmp as cmp","e.minimumQuantity as minimumQuantity","e.reorderQuantity as reorderQuantity,e.updated as updated");
        $qb->addSelect("item.itemCode as itemCode","item.name as name");
        $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
        $qb->addSelect("unit.name as unitName");
        $qb->addSelect("category.name as categoryName");
        $qb->addSelect("size.name as sizeName");
        $qb->addSelect("color.name as colorName");
        $qb->addSelect("brand.name as brandName");
        $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere("e.remainingQuantity <= e.minimumQuantity");
        if(!empty($data['items'])){
            $qb->andWhere("e.id IN(:ids)")->setParameter('ids', $data['items']);
        }
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("item.name",'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return  $result;
    }
    
    public function insertMasterStockItem(Item $item)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $item));
        $stockBookId = "i{$stock->getId()}";
        $exist = $this->findOneBy(array('stockBookId'=> "{$stockBookId}"));
        if(empty($exist)){
            $entity = new StockBook();
            $entity->setConfig($stock->getConfig());
            $entity->setStockBookId($stockBookId);
            $entity->setItem($item);
            $entity->setStock($stock);
            $entity->setCategory($item->getCategory());
            $entity->setMinimumQuantity($item->getMinQuantity());
            $entity->setReorderQuantity($item->getReorderQuantity());
            $entity->setPrice($item->getPurchasePrice());
            $entity->setCmp($item->getPurchasePrice());
            $em->persist($entity);
            $em->flush();
        }

    }

    public function insertStockItem(Item $item, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $brand = $data['brand'];
        $size = $data['size'];
        $color = $data['color'];
        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $item));
        if(!empty($brand) and !empty($size) and !empty($color)){
            $stockBookId = "i{$stock->getId()}-b{$brand}-s{$size}-c{$color}";
        }elseif(!empty($brand) and !empty($size)){
            $stockBookId = "i{$stock->getId()}-b{$brand}-s{$size}";
        }elseif(!empty($brand) and !empty($color)){
            $stockBookId = "i{$stock->getId()}-b{$brand}-c{$color}";
        }elseif(!empty($size) and !empty($color)){
            $stockBookId = "i{$stock->getId()}-s{$size}-c{$color}";
        }elseif(!empty($brand)){
            $stockBookId = "i{$stock->getId()}-b{$brand}";
        }elseif(!empty($size)){
            $stockBookId = "i{$stock->getId()}-s{$size}";
        }elseif(!empty($color)){
            $stockBookId = "i{$stock->getId()}-c{$color}";
        }else{
            $stockBookId = "i{$stock->getId()}";
        }
        
        $exist = $this->findOneBy(array('stockBookId'=> "{$stockBookId}"));
        if(empty($exist)){
            $entity = new StockBook();
            $entity->setConfig($stock->getConfig());
            $entity->setStockBookId($stockBookId);
            $entity->setItem($item);
            $entity->setStock($stock);
            $entity->setCategory($item->getCategory());
            if(!empty($brand)){
                $brandItem = $this->_em->getRepository(ItemBrand::class)->find($brand);
                $entity->setBrand($brandItem);
            }
            if(!empty($size)){
                $sizeItem = $this->_em->getRepository(ItemSize::class)->find($size);
                $entity->setSize($sizeItem);
            }
            if(!empty($color)){
                $colorItem = $this->_em->getRepository(ItemColor::class)->find($color);
                $entity->setColor($colorItem);
            }
            $minimumQuantity = (isset($data['minimumQuantity']) and $data['minimumQuantity']) ? $data['minimumQuantity'] : 0;
            $reorderQuantity = (isset($data['reorderQuantity']) and $data['reorderQuantity']) ? $data['reorderQuantity'] : 0;
            $price = (isset($data['price']) and $data['price']) ? $data['price'] : 0;
            $entity->setMinimumQuantity($minimumQuantity);
            $entity->setReorderQuantity($reorderQuantity);
            $entity->setPrice($price);
            $entity->setCmp($price);
            $em->persist($entity);
            $em->flush();
        }

    }

    public function jobApprovalStockBookItem(Stock $stock)
    {

        $em = $this->_em;
        $stockBookId = "i{$stock->getId()}";
        $exist = $this->findOneBy(array('stockBookId'=> "{$stockBookId}"));
        if(empty($exist)){
            $entity = new StockBook();
            $entity->setConfig($stock->getConfig());
            $entity->setStockBookId($stockBookId);
            $entity->setItem($stock->getItem());
            $entity->setStock($stock);
            $entity->setCategory($stock->getItem()->getCategory());
            if(!empty($brand)){
                $brandItem = $this->_em->getRepository(ItemBrand::class)->find($brand);
                $entity->setBrand($brandItem);
            }
            if(!empty($size)){
                $sizeItem = $this->_em->getRepository(ItemSize::class)->find($size);
                $entity->setSize($sizeItem);
            }
            if(!empty($color)){
                $colorItem = $this->_em->getRepository(ItemColor::class)->find($color);
                $entity->setColor($colorItem);
            }
            $em->persist($entity);
            $em->flush();
            return  $entity;
        }else{
            return $exist;
        }

    }

    public function stockBookMatrix(Stock $stock, $data)
    {
        $brand =  (isset($data['brand']) and $data['brand']) ? $data['brand']:'';
        $size =  (isset($data['size']) and $data['size']) ? $data['size']:'';
        $color =  (isset($data['color']) and $data['color']) ? $data['color']:'';

        if(!empty($brand) and !empty($size) and !empty($color)){
            $array = array('stock' => $stock,'brand' => $brand,'size' => $size,'color' => $color,'isDelete' => 0);
        }elseif(!empty($brand) and !empty($size)){
            $array = array('stock' => $stock,'brand' => $brand,'size' => $size,'color' => null,'isDelete' => 0);
        }elseif(!empty($brand) and !empty($color)){
            $array = array('stock' => $stock,'brand' => $brand,'size' => null,'color' => $color,'isDelete' => 0);
        }elseif(!empty($size) and !empty($color)){
            $array = array('stock' => $stock,'brand' => null,'size' => $size,'color' => $color,'isDelete' => 0);
        }elseif(!empty($brand)){
            $array = array('stock' => $stock,'brand' => $brand,'size' => null,'color' => null,'isDelete' => 0);
        }elseif(!empty($size)){
            $array = array('stock' => $stock,'brand' => null,'size' => $size,'color' => null,'isDelete' => 0);
        }elseif(!empty($color)){
            $array = array('stock' => $stock,'brand' => null,'size' => null,'color' => $color,'isDelete' => 0);
        }else{
            $array = array('stock' => $stock,'brand' => null,'size' => null,'color' => null,'isDelete' => 0);
        }
        return $array;
    }

    public function existStockBookItem(Stock $stock, $data)
    {

        $em = $this->_em;
        $exist = "";
        $array = $this->stockBookMatrix($stock, $data);
        $exist = $this->findOneBy($array);
        if(empty($exist)){
            $entity = new StockBook();
            $entity->setConfig($stock->getConfig());
            $stockBookId = time();
            $entity->setStockBookId($stockBookId);
            $entity->setItem($stock->getItem());
            $entity->setStock($stock);
            $entity->setCategory($stock->getItem()->getCategory());
            if(!empty($brand)){
                $brandItem = $this->_em->getRepository(ItemBrand::class)->find($brand);
                $entity->setBrand($brandItem);
            }
            if(!empty($size)){
                $sizeItem = $this->_em->getRepository(ItemSize::class)->find($size);
                $entity->setSize($sizeItem);
            }
            if(!empty($color)){
                $colorItem = $this->_em->getRepository(ItemColor::class)->find($color);
                $entity->setColor($colorItem);
            }
            $em->persist($entity);
            $em->flush();
            return  $entity;
        }else{
            return $exist;
        }

    }

    public function existPurchaseStockBookItem(PurchaseItem $purchaseItem)
    {
        $stock = $purchaseItem->getStock();
        $em = $this->_em;
        $array = $this->stockBookMatrix($stock, $data);
        $exist = $this->findOneBy($array);
        if(empty($exist)){
            $entity = new StockBook();
            $entity->setConfig($stock->getConfig());
            $entity->setStockBookId($stockBookId);
            $entity->setItem($stock->getItem());
            $entity->setStock($stock);
            $entity->setCategory($stock->getItem()->getCategory());
            if(!empty($brand)){
                $brandItem = $this->_em->getRepository(ItemBrand::class)->find($brand);
                $entity->setBrand($brandItem);
            }
            if(!empty($size)){
                $sizeItem = $this->_em->getRepository(ItemSize::class)->find($size);
                $entity->setSize($sizeItem);
            }
            if(!empty($color)){
                $colorItem = $this->_em->getRepository(ItemColor::class)->find($color);
                $entity->setColor($colorItem);
            }
            $em->persist($entity);
            $em->flush();
            return  $entity;
        }else{
            return $exist;
        }

    }

    public function searchStockBookItemAutoComplete($config,$q,$mode = "")
    {
        $query = $this->createQueryBuilder('e');
        $query->join('e.item','item');
        $query->leftJoin('item.unit','u');
        $query->leftJoin('e.size','s');
        $query->leftJoin('e.color','c');
        $query->leftJoin('e.brand','b');
        $query->select('e.id as id','CONCAT(item.itemCode, \' - \', item.name) AS text');
        $query->addSelect('s.name as size');
        $query->addSelect('c.name as color');
        $query->addSelect('b.name as brand');
        $query->addSelect('u.name as unit');
        $query->where("item.config = :config")->setParameter('config',$config);
        $query->andWhere('item.itemCode LIKE :searchTerm OR item.name LIKE :searchTerm');
        $query->setParameter('searchTerm', '%'.trim($q).'%');
        if($mode){
            $query->andWhere("item.itemMode = '{$mode}'");
        }
        $query->andWhere('item.status = 1');
        $query->andWhere("item.isDelete = 0");
        $query->orderBy('item.name', 'ASC');
        $query->setMaxResults( '250' );
        $result =  $query->getQuery()->getArrayResult();
        $entities = array();
        foreach ($result as $row){
            $name = "{$row['text']} {$row['brand']}{$row['size']}{$row['color']} {$row['unit']} ";
            $entities[$row['id']] = $name;
        }
        return $entities;

    }

    public function searchItemAutoComplete($config,$q,$mode = "")
    {
        $query = $this->_em->createQueryBuilder();
        $query->from(Item::class,'item');
        //$query->addSelect('item.id as id','item.name AS text');
        $query->addSelect('item.id as id','CONCAT(item.itemCode, \' - \', item.name) AS text');
        $query->where("item.config = :config")->setParameter('config',$config);
        $query->andWhere('item.itemCode LIKE :searchTerm OR item.name LIKE :searchTerm');
        $query->setParameter('searchTerm', '%'.trim($q).'%');
        if($mode){
            $query->andWhere("item.itemMode = '{$mode}'");
        }
        $query->andWhere('item.status = 1');
        $query->andWhere("item.isDelete = 0");
        $query->orderBy('item.name', 'ASC');
        $query->setMaxResults( '500' );
        $result =  $query->getQuery()->getArrayResult();
        $entities = array();
        foreach ($result as $row){
            $entities[$row['id']] = $row['text'];
        }
        return $entities;

    }

    public function searchDepartmentItemAutoComplete($config,$q,$department,$mode)
    {

        $query = $this->_em->createQueryBuilder();
        $query->from(Item::class,'item');
        $query->join('item.category','c');
        $query->leftJoin('c.generalLedger','gl');
        $query->leftJoin('gl.department','department');
        $query->addSelect('item.id as id','CONCAT(item.name, \' (\', c.name,\')\') AS text');
        $query->where("item.config = :config")->setParameter('config',$config);
        if( $mode != "CAPEX"){
            $query->andWhere("department.id = :departmentId")->setParameter('departmentId',$department->getid());
            $query->andWhere("item.itemMode = :itemMode")->setParameter('itemMode',"OPEX");
        }else{
            $query->andWhere("item.itemMode = :itemMode")->setParameter('itemMode',"CAPEX");
        }
        $query->andWhere($query->expr()->like("item.name", "'%$q%'"  ));
        $query->andWhere('item.status = 1');
        $query->andWhere("item.isDelete = 0");
        $query->orderBy('item.name', 'ASC');
        $query->setMaxResults( '50' );
        $result =  $query->getQuery()->getArrayResult();
        $entities = array();
        foreach ($result as $row){
            $entities[$row['id']] = $row['text'];
        }
        return $entities;

    }

    public function updateItemPrice(TenderWorkorder $workorder){

        $em = $this->_em;
        /** @var  $purchaseItem RequisitionOrderItem */
        if(!empty($workorder->getWorkOrderItems())) {
            /* @var $orderItem TenderWorkorderItem */
            foreach ( $workorder->getWorkOrderItems() as $orderItem) {
                $stockBook = $orderItem->getStockBook();
                if($orderItem->getPrice() > 0){
                    $em->getRepository(CmpPrice::class)->insertWoPrice($workorder,$stockBook,$orderItem->getPrice());
                    $stockBook->setPrice($orderItem->getPrice());
                    $stockBook->setCmp($orderItem->getPrice());
                    $stockBook->setCmpDate($workorder->getUpdated());
                }
                $em->persist($stockBook);
                $em->flush();
            }

        }
    }

    public function stockOutRequisitionOrderStockQuantity(RequisitionOrder $purchase){

        $em = $this->_em;
        $items = $this->_em->getRepository(RequisitionOrderItem::class)->findBy(array('requisitionOrder'=>$purchase));
        /** @var  $purchaseItem RequisitionOrderItem */
        if(!empty($items)) {
            foreach ( $items as $purchaseItem) {
                if($purchaseItem->getStockBook()){
                    $this->updateRemoveRequisitionOrderReceiveQuantity($purchaseItem->getStockBook());
                    $em->getRepository(StockHistory::class)->processOrderIssueQuantity($purchaseItem);
                }
            }
        }
    }

    public function insertWorkorderStockQuantity(TenderWorkorderReceive $purchase){

        $em =  $this->_em;
        $items = $this->_em->getRepository(TenderWorkorderReceiveItem::class)->findBy(array('workorderReceive' => $purchase));
        /** @var  $purchaseItem TenderWorkorderReceiveItem */
        if(!empty($items)) {
            foreach ( $items as $purchaseItem) {
                if($purchaseItem->getStockBook()){
                    $this->updateRemoveWorkOrderReceiveQuantity($purchaseItem);
                    $em->getRepository(StockHistory::class)->processWorkorderStockQuantity($purchaseItem);
                    $em->getRepository(StockWearhouse::class)->updateRemoveWorkOrderReceiveQuantity($purchaseItem);
                }
            }
        }
    }

    public function insertDirectReceiveStockQuantity(TenderWorkorderReceive $purchase){

        $em =  $this->_em;
        $items = $this->_em->getRepository(TenderWorkorderReceiveItem::class)->findBy(array('workorderReceive' => $purchase));
        /** @var  $purchaseItem TenderWorkorderReceiveItem */
        if(!empty($items)) {
            foreach ( $items as $purchaseItem) {
                if($purchaseItem->getStockBook()){
                    $this->updateRemoveWorkOrderReceiveQuantity($purchaseItem);
                    $em->getRepository(StockHistory::class)->processDirectStockReceiveQuantity($purchaseItem);
                    $em->getRepository(StockWearhouse::class)->updateRemoveDirectReceiveQuantity($purchaseItem);
                }
            }
        }
    }

    public function updateRemoveRequisitionOrderReceiveQuantity(StockBook $stock){

        $em = $this->_em;
        $qnt = $em->getRepository(RequisitionOrderItem::class)->requisitionOrderStockBookItemUpdate($stock);
        $stock->setStockOut($qnt);
        $em->persist($stock);
        $em->flush();
        $this->remainingQnt($stock);
    }

    public function updateRemoveWorkOrderReceiveQuantity(TenderWorkorderReceiveItem $purchaseItem){

        $stock = $purchaseItem->getStockBook();
        $em = $this->_em;
        $qnt = $em->getRepository(TenderWorkorderReceiveItem::class)->wordorderStockBookItemUpdate($stock);
        $stock->setStockIn($qnt);
        $stock->setPrice($purchaseItem->getPrice());
        $em->persist($stock);
        $em->flush();
        $this->remainingQnt($stock);
    }

    public function processPurchaseUpdateQnt(Purchase $purchase){

        /** @var  $purchaseItem PurchaseItem */
        if(!empty($purchase->getPurchaseItems())) {
            foreach ($purchase->getPurchaseItems() as $purchaseItem) {
                if($purchaseItem->getStockBook()){
                    $this->updateRemovePurchaseQuantity($purchaseItem->getStockBook());
                }
            }
        }
    }

    public function updateRemovePurchaseQuantity(StockBook $stock){

        $em = $this->_em;
        $qnt = $em->getRepository(PurchaseItem::class)->purchaseStockBookItemUpdate($stock);
        $stock->setStockIn($qnt);
        $em->persist($stock);
        $em->flush();
        $this->remainingQnt($stock);
    }

    public function remainingQnt(StockBook $stock)
    {
        $em = $this->_em;
        $stockIn = $stock->getStockIn();
        $stockOut = $stock->getStockOut();
        $stock->setStockIn($stockIn);
        $stock->setStockOut($stockOut);
        $stock->setRemainingQuantity($stockIn - $stockOut);
        $em->persist($stock);
        $em->flush();
        
    }

    public function getItemBrands(Item $stock)
    {

        $query = $this->createQueryBuilder('e');
        $query->select('b.id as id','b.name as name');
        $query->join('e.brand','b');
        $query->where("e.item = :item")->setParameter('item',$stock->getId());
        $query->andWhere('e.isDelete = 0');
        $query->orderBy('b.name', 'ASC');
        $query->groupBy('b.id');
        $result =  $query->getQuery()->getArrayResult();
        return $result;

    }

    public function getItemSizes(Item $stock)
    {
        $query = $this->createQueryBuilder('e');
        $query->select('b.id as id','b.name as name');
        $query->join('e.size','b');
        $query->where("e.item = :item")->setParameter('item',$stock->getId());
        $query->andWhere('e.isDelete = 0');
        $query->orderBy('b.name', 'ASC');
        $query->groupBy('b.id');
        $result =  $query->getQuery()->getArrayResult();
        return $result;

    }

    public function getItemColors(Item $stock)
    {
        $query = $this->createQueryBuilder('e');
        $query->select('b.id as id','b.name as name');
        $query->join('e.color','b');
        $query->where("e.item = :item")->setParameter('item',$stock->getId());
        $query->andWhere('e.isDelete = 0');
        $query->orderBy('b.name', 'ASC');
        $query->groupBy('b.id');
        $result =  $query->getQuery()->getArrayResult();
        return $result;

    }




}
