<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceive;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class StockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stock::class);
    }

    public function insertStockItem($config , Item $item)
    {
        $em = $this->_em;
        $stock = new Stock();
        $stock->setConfig($config);
        $stock->setItem($item);
        $stock->setStatus(true);
        $em->persist($stock);
        $em->flush();

    }

    public function insertImportStockItem($config , Item $item)
    {
        $em = $this->_em;
        $stock = new Stock();
        $stock->setConfig($config);
        $stock->setItem($item);
        $stock->setStatus(true);
        $em->persist($stock);
        $em->flush();

        $stockBook = new StockBook();
        $stockBook->setConfig($stock->getConfig());
        $stockBookId = "i{$stock->getId()}";
        $stockBook->setStockBookId($stockBookId);
        $stockBook->setItem($item);
        $stockBook->setStock($stock);
        $stockBook->setCategory($item->getCategory());
        $stockBook->setPrice($item->getPurchasePrice());
        $em->persist($stockBook);
        $em->flush();

    }

    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['inventory_filter_form'])) {
            $data = $form['inventory_filter_form'];
            $productGroup           = isset($data['productGroup'])? $data['productGroup'] :'';
            $category               = isset($data['category'])? $data['category'] :'';
            $itemCode               = isset($data['itemCode'])? $data['itemCode'] :'';
            $keyword                = isset($data['keyword'])? $data['keyword'] :'';
            $itemMode                = isset($data['itemMode'])? $data['itemMode'] :'';
            $item               = isset($data['name'])? $data['name'] :'';
            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if (!empty($itemMode)) {
                $qb->andWhere("item.itemMode = :itemMode")->setParameter('itemMode', $itemMode);
            }
            if (!empty($item)) {
                $qb->andWhere('item.name LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$item.'%');
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($productGroup)) {
                $qb->andWhere("productGroup.id = :pid")->setParameter('pid', $productGroup);
            }
            if (!empty($keyword)) {
                $qb->andWhere('category.name LIKE :searchTerm OR item.name LIKE :searchTerm OR item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.trim($keyword).'%');
            }
        }
    }
    public function findBySearchQuery( $config, $data )
    {

        $keyword               = isset($data['keyword'])? $data['keyword'] :'';
        $item               = isset($data['name'])? $data['name'] :'';
        $productGroup       = isset($data['groupName'])? $data['groupName'] :'';
        $productType        = isset($data['productType'])? $data['productType'] :'';
        $category           = isset($data['category'])? $data['category'] :'';
        $itemMode           = isset($data['itemMode'])? $data['itemMode'] :'';
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.item','item');
        $qb->leftJoin('item.brand','b');
        $qb->leftJoin('item.productGroup','productGroup');
        $qb->leftJoin('item.priceMethod','priceMethod');
        $qb->leftJoin('item.category','category');
        $qb->leftJoin('item.unit','unit');
        $qb->select("e.id as id","item.itemCode as itemCode","item.name as name","e.status as status","e.purchasePrice as purchasePrice","e.salesPrice as salesPrice");
        $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
        $qb->addSelect("e.purchaseQuantity as purchaseQuantity");
        $qb->addSelect("e.remainingQuantity as remainingQuantity","e.stockIn as stockIn","e.stockOut as stockOut");
        $qb->addSelect("unit.name as unitName");
        $qb->addSelect("category.name as categoryName");
        $qb->addSelect("productGroup.name as groupName");
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere("e.isDelete = 0");
        $this->handleSearchBetween($qb,$data);
        $qb->orderBy("item.name",'ASC');
       // $qb->setMaxResults(100);
        $result = $qb->getQuery();
        return  $result;

    }

    public function insertBankWorkorderStockQuantity(TenderWorkorderReceive $purchase){

        $em =  $this->_em;
        $items = $this->_em->getRepository(TenderWorkorderReceiveItem::class)->findBy(array('workorderReceive' => $purchase));
        /** @var  $purchaseItem TenderWorkorderReceiveItem */
        if(!empty($items)) {
            foreach ( $items as $purchaseItem) {
                if($purchaseItem->getStock()){
                    $this->updateRemoveWorkOrderReceiveQuantity($purchaseItem);
                    $em->getRepository(StockHistory::class)->processBankWorkorderStockQuantity($purchaseItem);
                   // $em->getRepository(StockWearhouse::class)->updateRemoveWorkOrderReceiveQuantity($purchaseItem);
                }
            }
        }
    }

    public function insertGarmentWorkorderStockQuantity(TenderWorkorderReceive $purchase){

        $em =  $this->_em;
        $items = $em->getRepository(TenderWorkorderReceiveItem::class)->findBy(array('workorderReceive' => $purchase));
        /** @var  $purchaseItem TenderWorkorderReceiveItem */
        if(!empty($items)) {
            foreach ( $items as $purchaseItem) {
                if($purchaseItem->getStock()){
                    $this->updateRemoveWorkOrderReceiveQuantity($purchaseItem);
                }
            }
        }
    }

    public function updateRemoveWorkOrderReceiveQuantity(TenderWorkorderReceiveItem $purchaseItem){

        $em = $this->_em;
        $stock = $purchaseItem->getStock();
        $qnt = $em->getRepository(TenderWorkorderReceiveItem::class)->wordorderStockItemUpdate($stock);
        $stock->setStockIn($qnt);
        $stock->setPurchasePrice($purchaseItem->getPrice());
        $em->persist($stock);
        $em->flush();
        $this->remainingQnt($stock);
    }


    public function updateRemovePurchaseQuantity(Stock $stock , $fieldName = ''){

        $em = $this->_em;
        if($fieldName == 'requisitionOrder'){
            $quantity = $em->getRepository(RequisitionOrderItem::class)->orderItemStockItemUpdate($stock);
            $stock->setRequisitionOrderQuantity($quantity);
        }elseif($fieldName == 'goods-receive'){
            $quantity = $em->getRepository(TenderWorkorderReceiveItem::class)->orderItemStockItemUpdate($stock);
            $stock->setSalesReturnQuantity($quantity);
        }else{
            $qnt = $em->getRepository(PurchaseItem::class)->purchaseStockItemUpdate($stock);
            $stock->setStockIn($qnt);
        }
        $em->persist($stock);
        $em->flush();
        $this->remainingQnt($stock);
    }

    public function remainingQnt(Stock $stock)
    {
        $em = $this->_em;
        $stock->setRemainingQuantity($stock->getStockIn() - $stock->getStockOut());
        $em->persist($stock);
        $em->flush();
    }

    public function processPurchaseUpdateQnt(Purchase $purchase){

        /** @var  $purchaseItem PurchaseItem */

        if(!empty($purchase->getPurchaseItems())) {
            foreach ($purchase->getPurchaseItems() as $purchaseItem) {
                $stockItem = $purchaseItem->getStock();
                $this->updateRemovePurchaseQuantity($stockItem);
                $this->updatePurchasePrice($stockItem,$purchaseItem);
            }
        }
    }


    public function processRequisitionOrderUpdateQnt(RequisitionOrder $purchase){

        /** @var  $purchaseItem RequisitionOrderItem */

        if(!empty($purchase->getRequisitionOrderItems())) {
            foreach ($purchase->getRequisitionOrderItems() as $purchaseItem) {
                $stockItem = $purchaseItem->getRequisitionItem()->getStock();
                $this->updateRemovePurchaseQuantity($stockItem,"requisitionOrder");
            }
        }

    }


    public function updatePurchasePrice(Stock $stock,PurchaseItem $item)
    {
        $em = $this->_em;
        $avg = $em->getRepository(PurchaseItem::class)->getPurchaseSalesAvg($stock);
        $stock->setPurchasePrice($item->getPrice());
        $stock->setAveragePurchasePrice($avg['purchase']);
        $em->persist($stock);
        $em->flush();
    }




}
