<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use App\Entity\Domain\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\Item;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\InventoryBundle\Entity\StockHistory;
use Terminalbd\InventoryBundle\Entity\StockWearhouse;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrder;
use Terminalbd\ProcurementBundle\Entity\RequisitionOrderItem;
use Terminalbd\ProcurementBundle\Entity\TenderWorkorderReceiveItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class StockWearhouseRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockWearhouse::class);
    }


    protected function handleSearchBetween($qb, $form)
    {

        if (isset($form['inventory_filter_form'])) {
            $data = $form['inventory_filter_form'];
            $productGroup           = isset($data['productGroup'])? $data['productGroup'] :'';
            $category               = isset($data['category'])? $data['category'] :'';
            $itemCode               = isset($data['itemCode'])? $data['itemCode'] :'';
            $keyword                = isset($data['keyword'])? $data['keyword'] :'';
            $wearhouse              = isset($data['wearhouse'])? $data['wearhouse'] :'';
            $itemMode               = isset($data['itemMode'])? $data['itemMode'] :'';

            if (!empty($category)) {
                $qb->andWhere("category.id = :cid")->setParameter('cid', $category);
            }
            if($wearhouse){
                $qb->andWhere("store.id = :wearhouse")->setParameter('wearhouse', $wearhouse);
            }
            if (!empty($itemMode)) {
                $qb->andWhere('item.itemMode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemMode.'%');
            }
            if (!empty($itemCode)) {
                $qb->andWhere('item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$itemCode.'%');
            }
            if (!empty($productGroup)) {
                $qb->andWhere("productGroup.id = :pid")->setParameter('pid', $productGroup);
            }
            if (!empty($keyword)) {
                $qb->andWhere('category.name LIKE :searchTerm OR item.name LIKE :searchTerm OR item.itemCode LIKE :searchTerm');
                $qb->setParameter('searchTerm', '%'.$keyword.'%');
            }
        }
    }
    public function findBySearchQuery( $config, $data ): array
    {
        $sort = isset($data['sort'])? $data['sort'] :'e.id';
        $direction = isset($data['direction'])? $data['direction'] :'DESC';
        if (isset($data['inventory_filter_form'])) {
            $form = $data['inventory_filter_form'];
            $wearhouse = isset($form['wearhouse'])? $form['wearhouse'] :'';
            $qb = $this->createQueryBuilder('wh');
            $qb->join('wh.wearhouse','store');
            $qb->leftJoin('wh.stockBook','e');
            $qb->leftJoin('e.item','item');
            $qb->leftJoin('e.brand','b');
            $qb->leftJoin('e.category','category');
            $qb->leftJoin('category.generalLedger','gl');
            $qb->leftJoin('e.brand','brand');
            $qb->leftJoin('e.size','size');
            $qb->leftJoin('e.color','color');
            $qb->leftJoin('item.unit','unit');
            $qb->select("wh.id as id","(wh.stockIn - wh.stockOut) as remainingQuantity","wh.stockIn as stockIn","wh.stockOut as stockOut","wh.minimumQuantity as minimumQuantity","wh.reorderQuantity as reorderQuantity");
            $qb->addSelect("e.price as price","e.cmp as cmp","e.updated as updated");
            $qb->addSelect("item.itemCode as itemCode","item.name as name");
            $qb->addSelect("store.id as wearhouse","store.name as storeName");
            $qb->addSelect("item.id as itemId");
            $qb->addSelect("unit.name as unitName");
            $qb->addSelect("category.name as categoryName");
            $qb->addSelect("size.name as sizeName");
            $qb->addSelect("color.name as colorName");
            $qb->addSelect("brand.name as brandName");
            $qb->addSelect("gl.name as glName","gl.generalLedgerCode as glCode");
        //    $qb->where("e.config = :config")->setParameter('config', $config->getId());
         //   $qb->where("e.isDelete = 0");
            if(!empty($data['items'])){
                $qb->andWhere($qb->expr()->in("e.id", $data['items'] ));
            }
            $this->handleSearchBetween($qb,$data);
            $qb->orderBy("item.name",'ASC');
            $result = $qb->getQuery()->getArrayResult();
            return $result;
        }
        return array();

    }

    public function findByKanbanPurchaseRequisitionItem( $config,$store, $items ): array
    {

        $qb = $this->createQueryBuilder('wh');
        $qb->join('wh.wearhouse', 'store');
        $qb->leftJoin('wh.stockBook', 'e');
        $qb->leftJoin('e.item','item');
        $qb->leftJoin('e.brand','b');
        $qb->leftJoin('e.brand','brand');
        $qb->leftJoin('e.size','size');
        $qb->leftJoin('e.color','color');
        $qb->leftJoin('item.unit','unit');
        $qb->select("e.id as id");
        $qb->select("e.id as id","e.remainingQuantity as remainingQuantity","e.stockIn as stockIn","e.stockOut as stockOut","e.price as price","e.cmp as cmp","e.minimumQuantity as minimumQuantity","e.reorderQuantity as reorderQuantity,e.updated as updated");
        $qb->addSelect("item.itemCode as itemCode","item.name as name");
        $qb->addSelect("item.id as itemId","item.itemMode as itemMode");
        $qb->addSelect("unit.name as unitName");
        $qb->addSelect("size.name as sizeName");
        $qb->addSelect("color.name as colorName");
        $qb->addSelect("brand.name as brandName");
        $qb->where("e.config = :config")->setParameter('config', $config->getId());
        $qb->andWhere("store.id = :branch")->setParameter('branch', $store);
        if(!empty($items)){
            $qb->andWhere("e.id IN(:ids)")->setParameter('ids',$items);
        }
        $qb->orderBy("item.name",'ASC');
        $result = $qb->getQuery()->getArrayResult();
        return  $result;
    }

    public function findByKanbanQuery( $config, $data , $mode ): array
    {
        if (isset($data['inventory_filter_form'])) {
            $form = $data['inventory_filter_form'];
            $wearhouse = isset($form['wearhouse']) ? $form['wearhouse'] : '';
            if ($wearhouse) {
                $sort = isset($data['sort']) ? $data['sort'] : 'e.id';
                $direction = isset($data['direction']) ? $data['direction'] : 'DESC';
                $qb = $this->createQueryBuilder('wh');
                $qb->join('wh.wearhouse', 'store');
                $qb->leftJoin('wh.stockBook', 'e');
                $qb->leftJoin('e.item', 'item');
                $qb->leftJoin('e.brand', 'b');
                $qb->leftJoin('e.category', 'category');
                $qb->leftJoin('category.generalLedger', 'gl');
                $qb->leftJoin('e.brand', 'brand');
                $qb->leftJoin('e.size', 'size');
                $qb->leftJoin('e.color', 'color');
                $qb->leftJoin('item.unit', 'unit');
                $qb->select("wh.remainingQuantity as remainingQuantity", "wh.stockIn as stockIn", "wh.stockOut as stockOut", "wh.minimumQuantity as minimumQuantity", "wh.reorderQuantity as reorderQuantity");
                $qb->addSelect("e.id as id", "e.price as price", "e.cmp as cmp", "e.updated as updated");
                $qb->addSelect("item.itemCode as itemCode", "item.name as name");
                $qb->addSelect("item.id as itemId", "item.itemMode as itemMode");
                $qb->addSelect("unit.name as unitName");
                $qb->addSelect("category.name as categoryName");
                $qb->addSelect("size.name as sizeName");
                $qb->addSelect("color.name as colorName");
                $qb->addSelect("brand.name as brandName");
                $qb->addSelect("gl.name as glName", "gl.generalLedgerCode as glCode");
                $qb->where("e.config = :config")->setParameter('config', $config->getId());
                if ($mode != 'list') {
                    $double = floatval($mode);
                    $qb->addSelect("((wh.minimumQuantity * $double) + wh.minimumQuantity) as minimumValue");
                    $qb->andWhere("wh.remainingQuantity >= wh.minimumQuantity");
                    $qb->having('wh.remainingQuantity <= minimumValue');
                } else {
                    $qb->andWhere("wh.remainingQuantity <= wh.minimumQuantity");
                }
                $this->handleSearchBetween($qb, $data);
                $qb->orderBy("item.name", 'ASC');
                $result = $qb->getQuery()->getArrayResult();
                return $result;
            }
        }
        return array();
    }

    public function updateRemoveWorkOrderReceiveQuantity(TenderWorkorderReceiveItem $item){

        $em = $this->_em;

        /* @var $wearhouse Branch */
        if($item->getWorkorderReceive()->getMode() == "Direct"){
            $wearhouse = $item->getWorkorderReceive()->getWearhouse();
        }else{
            $wearhouse = $item->getTenderWorkorderItem()->getWearhouse();
        }
        if($wearhouse){
            /* @var $stockWearhouse StockWearhouse */
            $stockWearhouse = $em->getRepository(StockWearhouse::class)->findOneBy(array('wearhouse'=>$wearhouse,'stockBook'=> $item->getStockBook()));
            $quantity = $em->getRepository(StockHistory::class)->stockReceiveSockBookItem($wearhouse->getId(),$item->getStockBook()->getId(),'receive');
            if($stockWearhouse){
                $stockWearhouse->setWearhouse($wearhouse);
                $stockWearhouse->setStock($item->getStockBook()->getStock());
                $stockWearhouse->setItem($item->getStockBook()->getItem());
                $stockWearhouse->setStockBook($item->getStockBook());
                $stockWearhouse->setStockIn($quantity);
                $em->persist($stockWearhouse);
                $em->flush();
                $this->remainingQnt($stockWearhouse);
            }else{
                $stockWearhouse = new StockWearhouse();
                $stockWearhouse->setWearhouse($wearhouse);
                $stockWearhouse->setStock($item->getStockBook()->getStock());
                $stockWearhouse->setItem($item->getStockBook()->getItem());
                $stockWearhouse->setStockBook($item->getStockBook());
                $stockWearhouse->setStockIn($quantity);
                $em->persist($stockWearhouse);
                $em->flush();
                $this->remainingQnt($stockWearhouse);
            }
        }

    }

    public function updateRemoveDirectReceiveQuantity(TenderWorkorderReceiveItem $item){

        $em = $this->_em;

        /* @var $wearhouse Branch */
        $wearhouse = $item->getWorkorderReceive()->getWearhouse();

        /* @var $stockWearhouse StockWearhouse */

        if($wearhouse){
            $stockWearhouse = $em->getRepository(StockWearhouse::class)->findOneBy(array('wearhouse'=>$wearhouse,'stockBook'=> $item->getStockBook()));
            $quantity = $em->getRepository(StockHistory::class)->stockReceiveSockBookItem($wearhouse->getId(),$item->getStockBook()->getId(),'receive');
            if($stockWearhouse){
                $stockWearhouse->setWearhouse($wearhouse);
                $stockWearhouse->setStock($item->getStockBook()->getStock());
                $stockWearhouse->setItem($item->getStockBook()->getItem());
                $stockWearhouse->setStockBook($item->getStockBook());
                $stockWearhouse->setStockIn($quantity);
                $em->persist($stockWearhouse);
                $em->flush();
                $this->remainingQnt($stockWearhouse);
            }else{
                $stockWearhouse = new StockWearhouse();
                $stockWearhouse->setWearhouse($wearhouse);
                $stockWearhouse->setStock($item->getStockBook()->getStock());
                $stockWearhouse->setItem($item->getStockBook()->getItem());
                $stockWearhouse->setStockBook($item->getStockBook());
                $stockWearhouse->setStockIn($quantity);
                $em->persist($stockWearhouse);
                $em->flush();
                $this->remainingQnt($stockWearhouse);
            }
        }

    }

    public function orderIssueWearhouse(RequisitionOrder $order)
    {
        foreach ($order->getRequisitionOrderItems() as $item){
           $this->updateRemoveIssueQuantity($item);
        }
    }

    public function updateRemoveIssueQuantity(RequisitionOrderItem $item){

        $em = $this->_em;
        /* @var $wearhouse Branch */
        $wearhouse = $item->getRequisitionOrder()->getRequisitionIssue()->getWearhouse();
        /* @var $stockWearhouse StockWearhouse */
        $stockWearhouse = $em->getRepository(StockWearhouse::class)->findOneBy(array('wearhouse'=>$wearhouse,'stockBook'=> $item->getStockBook()));
        $quantity = $em->getRepository(StockHistory::class)->stockIssueStockBookItem($wearhouse->getId(),$item->getStockBook()->getId());
        if($stockWearhouse){
            $stockWearhouse->setWearhouse($wearhouse);
            $stockWearhouse->setStockOut($quantity);
            $em->persist($stockWearhouse);
            $em->flush();
            $this->remainingQnt($stockWearhouse);
        }
    }
    
    public function remainingQnt(StockWearhouse $stock)
    {
        $em = $this->_em;
        $stockIn = ($stock->getStockIn());
        $stockOut = ($stock->getStockOut());
        $stock->setStockIn($stockIn);
        $stock->setStockOut($stockOut);
        $stock->setRemainingQuantity($stockIn - $stockOut);
        $em->persist($stock);
        $em->flush();
    }

    public function findStockItems(RequisitionOrder $order )
    {
        $ids = array();
        $array = array();
        $items = $order->getRequisitionIssue()->getRequisitionItems();

        $wearhouse = $order->getRequisitionIssue()->getWearhouse()->getId();
        foreach ($items as $item){
            $ids[] = $item->getStockBook()->getId();
        }
        $qb = $this->createQueryBuilder('wh');
        $qb->join('wh.wearhouse','store');
        $qb->leftJoin('wh.stockBook','e');
        $qb->select("wh.id as id","(wh.stockIn - wh.stockOut) as stockQuantity");
        $qb->addSelect("store.id as wearhouse");
        $qb->addSelect("e.id as itemId");
        $qb->where("store.id = :storeId")->setParameter('storeId', $wearhouse);
        if(!empty($ids)){
            $qb->andWhere($qb->expr()->in("wh.stockBook", $ids));
        }
        $result = $qb->getQuery()->getArrayResult();
        foreach ($result as $row){
            $array[$row['itemId']] = $row['stockQuantity'];
        }
        return $array;
    }

}
