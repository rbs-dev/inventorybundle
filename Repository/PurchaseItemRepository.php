<?php

/*
 * This file is part of the Symfony package.
 *
 * (c) Fabien Potencier <fabien@symfony.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Terminalbd\InventoryBundle\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Query;
use Terminalbd\GenericBundle\Entity\ItemBrand;
use Terminalbd\GenericBundle\Entity\ItemColor;
use Terminalbd\GenericBundle\Entity\ItemSize;
use Terminalbd\InventoryBundle\Entity\Purchase;
use Terminalbd\InventoryBundle\Entity\PurchaseItem;
use Terminalbd\InventoryBundle\Entity\Stock;
use Terminalbd\InventoryBundle\Entity\StockBook;
use Terminalbd\ProcurementBundle\Entity\Requisition;
use Terminalbd\ProcurementBundle\Entity\RequisitionItem;

/**
 * This custom Doctrine repository contains some methods which are useful when
 * querying for blog post information.
 *
 * See https://symfony.com/doc/current/doctrine/repository.html
 *
 * @author Md Shafiqul islam <shafiqabs@gmail.com>
 */
class PurchaseItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PurchaseItem::class);
    }

    public function getItemSummary(Purchase $invoice)
    {
        $id = $invoice->getId();
        $em = $this->_em;
        $qb = $this->createQueryBuilder('e');
        $qb->select('SUM(e.subTotal) as subTotal');
        $qb->join('e.purchase','i');
        $qb->where("i.id = '{$id}'");
        $result = $qb->getQuery()->getOneOrNullResult();
        if(!empty($result) and !empty($result['subTotal'])){
            $subTotal = $result['subTotal'];
            $invoice->setSubTotal($subTotal);
            $invoice->setTotal($subTotal);
        }else{
            $invoice->setSubTotal(0);
            $invoice->setVat(0);
            $invoice->setTotal(0);
        }
        $em->persist($invoice);
        $em->flush();

    }

    public function getCalculationVat($totalAmount)
    {
        $vat = ( ($totalAmount * (int)10)/100 );
        //$vat = ( ($totalAmount * (int)$sales->getRestaurantConfig()->getVatPercentage())/100 );
        return round($vat);
    }

    public function insertStoreItem(Purchase $purchase, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
        if($stock and $data['quantity'] > 0){
            $exist = $this->findOneBy(array('purchase'=> $purchase,'stock' => $stock));
            if(empty($exist)){
                $entity = new PurchaseItem();
                $entity->setPurchase($purchase);
                $entity->setQuantity($data['quantity']);
                $entity->setStock($stock);
                $entity->setItem($stock->getItem());
                $entity->setName($stock->getItem()->getName());
                if($purchase->getInvoiceMode() == "purchase"){
                    $entity->setPrice($data['price']);
                }else{
                    $entity->setPrice($data['price']);
                    $entity->setPurchasePrice($data['price']);
                }
                $entity->setSubTotal($entity->getQuantity() * $entity->getPrice());
                $em->persist($entity);
                $em->flush();
            }else{
                $entity = $exist;
                $entity->setQuantity($data['quantity']);
                if($purchase->getInvoiceMode() == "purchase"){
                    $entity->setPrice($data['price']);
                }else{
                    $entity->setPrice($data['price']);
                    $entity->setPurchasePrice($data['price']);
                }
                $entity->setSubTotal($entity->getQuantity() * $entity->getPrice());
                $em->persist($entity);
                $em->flush();
            }

        }

    }

    public function insertIssueItem(Purchase $purchase, $data)
    {

        $em = $this->_em;

        /* @var $stock Stock */

        $stock = $em->getRepository(Stock::class)->findOneBy(array('item' => $data['item']));
        $stockBook = $em->getRepository(StockBook::class)->existStockBookItem($stock,$data);
        if($stock and !empty($stockBook) and $data['quantity'] > 0){
            $entity = new PurchaseItem();
            $entity->setPurchase($purchase);
            $entity->setQuantity($data['quantity']);
            $entity->setStock($stock);
            $entity->setStockBook($stockBook);
            $entity->setItem($stock->getItem());
            $entity->setName($stock->getItem()->getName());
            if($purchase->getInvoiceMode() == "purchase"){
                $entity->setPrice($data['price']);
            }else{
                $entity->setPrice($data['price']);
                $entity->setPurchasePrice($data['price']);
            }
            if($data['brand']){
                $brand = $em->getRepository(ItemBrand::class)->find($data['brand']);
                $entity->setBrand($brand);
            }
            if($data['size']){
                $size = $em->getRepository(ItemSize::class)->find($data['size']);
                $entity->setSize($size);
            }
            if($data['color']){
                $color = $em->getRepository(ItemColor::class)->find($data['color']);
                $entity->setColor($color);
            }
            $entity->setSubTotal($entity->getQuantity() * $entity->getPrice());
            $em->persist($entity);
            $em->flush();
        }

    }

    public function updateStoreItem(PurchaseItem $item,$quantity)
    {
        $em = $this->_em;
        $item->setQuantity($quantity);
        $item->setSubTotal($quantity * $item->getPrice());
        $em->persist($item);
        $em->flush();
    }

    public function purchaseStockItemUpdate(Stock $stockItem)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.purchase', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stock = :stock')->setParameter('stock', $stockItem->getId());
        $qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }

    public function purchaseStockBookItemUpdate(StockBook $stockItem)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.purchase', 'mp');
        $qb->select('SUM(e.quantity) AS quantity');
        $qb->where('e.stockBook = :stock')->setParameter('stock', $stockItem->getId());
        $qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $qnt = $qb->getQuery()->getOneOrNullResult();
        return $qnt['quantity'];
    }

    public function getPurchaseSalesAvg(Stock $stockItem)
    {
        $qb = $this->createQueryBuilder('e');
        $qb->join('e.purchase', 'mp');
        $qb->select('AVG(e.purchasePrice) AS purchase');
        $qb->where('e.stock = :stock')->setParameter('stock', $stockItem->getId());
        //$qb->andWhere('mp.process = :process')->setParameter('process', 'Approved');
        $avg = $qb->getQuery()->getOneOrNullResult();
        return $avg;
    }


}
